import React, { Component } from "react";
import { Platform, StyleSheet, Text, View, StatusBar } from "react-native";
import SplashScreen from "react-native-splash-screen";
import { Provider } from "react-redux";
import { createStore, applyMiddleware } from "redux";
import ReduxThunk from "redux-thunk";
import reducers from "./src/Reducers";
import { MainRouter, NavigationService } from "./src/navigator";
import firebase from "react-native-firebase";
import AsyncStorage from "@react-native-community/async-storage";
import type, { Notification } from "react-native-firebase";
import FlashMessage, {
  showMessage,
  hideMessage
} from "react-native-flash-message";
import { withTranslation } from "react-i18next";
import i18n from "./src/i18n/index";
import { SafeAreaView } from "react-navigation";

const Deferred = () => {
  var d = {};
  d.promise = new Promise(function(resolve, reject) {
    d.resolve = resolve;
    d.reject = reject;
  });
  return d;
};

const navigationDeferred = new Deferred();

// const MyStatusBar = ({ backgroundColor, ...props }) => (
//   <View style={[styles.statusBar, { backgroundColor }]}>
//     <StatusBar translucent backgroundColor={backgroundColor} {...props} />
//   </View>
// );

const AppWrapper = ({ t }) => (
  <MainRouter
    screenProps={{ t }}
    ref={navigatorRef => {
      NavigationService.setTopLevelNavigator(navigatorRef);
      navigationDeferred.resolve(navigatorRef);
    }}
  />
);

const MainApp = withTranslation("common", {
  bindI18n: "languageChanged",
  bindStore: false,
  wait: true
})(AppWrapper);

export default class App extends Component {
  constructor(props) {
    super(props);
    this.state = {
      notification: null
    };
  }

  async componentDidMount() {
    AsyncStorage.getItem("checkFirstTimeToken", (err, resFirstTime) => {
      if (resFirstTime == "firstDone") {
        NavigationService.navigate("Auth");
      } else {
        setTimeout(() => {
          SplashScreen.hide();
        }, 500);
      }
    });
    StatusBar.setBackgroundColor("rgb(41,34,108)");
    this.requestPermission();
    const fcmToken = await firebase.messaging().getToken();
    if (fcmToken) {
      console.log(fcmToken, "TOKENFCM");
      AsyncStorage.setItem("FCMTOKEN", fcmToken);
    } else {
      // user doesn't have a device token yet
    }
  }

  componentWillUnmount() {
    this.notificationOpenedListener();
    // this.notificationListener();
  }

  requestPermission = () => {
    firebase
      .messaging()
      .hasPermission()
      .then(enabled => {
        if (enabled) {
          // user has permissions
          this.createNotificationListeners();
          console.log("ENABLED", enabled);
        } else {
          firebase
            .messaging()
            .requestPermission()
            .then(() => {
              this.createNotificationListeners();
              console.log("ASKED FOR PERMISSIOM");
            })
            .catch(error => {
              console.log("ERROR PERMISSIOSM", error);
            });
        }
      });
  };

  async createNotificationListeners() {
    /*
     * Triggered when a particular notification has been received in foreground
     * */
    // this.notificationListener = firebase
    //   .notifications()
    //   .onNotification(notification => {
    //     // NavigationService.navigate(notification._data.url);
    //     NavigationService.navigate('Notifications');
    //   });

    /*
     * If your app is in background, you can listen for when a notification is clicked / tapped / opened as follows:
     * */
    this.notificationOpenedListener = firebase
      .notifications()
      .onNotificationOpened(async notificationOpen => {
        const notification: Notification = notificationOpen.notification;
        // NavigationService.navigate(notificationOpen.notification._data.url);
        NavigationService.navigate("Notifications");
        firebase
          .notifications()
          .removeDeliveredNotification(notification.notificationId);
        console.log("====================================");
        console.log(
          "heloo notification from Open Listener Function",
          notificationOpen
        );
        console.log("====================================");
      });

    /*
     * If your app is closed, you can check if it was opened by a notification being clicked / tapped / opened as follows:
     * */
    const notificationOpen = await firebase
      .notifications()
      .getInitialNotification();
    if (notificationOpen) {
      navigationDeferred.promise.then(() => {
        NavigationService.navigate("Notifications");
        console.log("====================================");
        console.log(
          "heloo notification from GET INITIAL NOTIFINATION",
          notificationOpen
        );
        console.log("====================================");
      });
    }
  }

  render() {
    const store = createStore(reducers, {}, applyMiddleware(ReduxThunk));
    return (
      <Provider store={store}>
        <StatusBar
          backgroundColor={"rgb(41,34,108)"}
          barStyle="light-content"
        />

        <MainApp />
      </Provider>
    );
  }
}

const STATUSBAR_HEIGHT = Platform.OS === "ios" ? 20 : StatusBar.currentHeight;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: "center",
    alignItems: "center",
    backgroundColor: "#F5FCFF"
  },
  statusBar: {
    height: STATUSBAR_HEIGHT
  },
  welcome: {
    fontSize: 20,
    textAlign: "center",
    margin: 10
  },
  instructions: {
    textAlign: "center",
    color: "#333333",
    marginBottom: 5
  }
});
