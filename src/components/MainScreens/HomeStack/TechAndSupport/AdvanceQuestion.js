import React, { Fragment, Component } from "react";
import {
  View,
  Text,
  ScrollView,
  ActivityIndicator,
  TouchableOpacity,
  StyleSheet
} from "react-native";
import { connect } from "react-redux";
import { TechSupportApi } from "../../../../config/api";
import { setBasicSettings } from "../../../../Actions/sprayparameters";
import QuestionPicker from "./QuestionPicker";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp
} from "react-native-responsive-screen";
class AdvanceQuestion extends Component {
  onPickerValueChange = value => {
    // this.props.setBasicSettings(value);
  };
  render() {
    const question = this.props.navigation.getParam("data");
    return (
      <Fragment>
        <ScrollView
          showsVerticalScrollIndicator={false}
          contentContainerStyle={{
            paddingBottom: 20
          }}
        >
          <Text style={styles.titleStyle}>Advance Settings</Text>

          {question.length > 0 ? (
            question.map(item => <QuestionPicker
              onValueChange={data => {
                this.onPickerValueChange(data);
              }}
              data={item} />)
          ) : (
            <ActivityIndicator
              size={"large"}
              color={"rgb(41,34,108)"}
              style={{ marginTop: "auto", marginBottom: "auto" }}
            />
          )}
          <TouchableOpacity
            onPress={() => this.props.navigation.navigate("SprayParameters")}
            style={styles.button}
          >
            <Text
              style={{
                color: "white",
                textAlign: "center",
                fontSize: 15,
                fontFamily: "Comfortaa-Bold"
                //   marginVertical : 10
              }}
            >
              Go Back
            </Text>
          </TouchableOpacity>
          <TouchableOpacity
            style={[
              styles.button,
              {
                backgroundColor: "white",
                borderWidth: 1,
                borderColor: "rgb(41,34,108)",
                marginVertical: 0,
                marginBottom: 10
              }
            ]}
          >
            <Text
              style={{
                textAlign: "center",
                fontSize: 15,
                fontFamily: "Comfortaa-Bold",
                color: "rgb(41,34,108)"
              }}
            >
              Get Recomendation
            </Text>
          </TouchableOpacity>
        </ScrollView>
      </Fragment>
    );
  }
}

const mapStateToProps = ({ auth }) => {
  const { accessToken } = auth;
  return { accessToken, basicQuestions };
};
const actionCreater = {
  setBasicSettings
};

export default connect(mapStateToProps, actionCreater)(AdvanceQuestion);
const styles = StyleSheet.create({
  titleStyle: {
    fontSize: 15,
    color: "rgb(51,51,51)",
    fontFamily: "Comfortaa-Bold",
    marginVertical: 10
  },
  question: {
    fontSize: 13,
    color: "rgb(51,51,51)",
    fontFamily: "Comfortaa-Bold",
    marginVertical: 10
  },
  viewContainer: {
    height: hp("5.5%"),
    width: wp("82%"),
    fontFamily: "Comfortaa-Bold",
    borderWidth: 0.6,
    marginLeft: "auto",
    marginRight: "auto",
    justifyContent: "center",
    borderColor: "rgba(200,200,200,1)",
    borderRadius: 5
  },
  button: {
    height: 47,
    width: "100%",
    borderRadius: 8,
    justifyContent: "center",
    alignSelf: "center",
    // alignItems: 'center',
    backgroundColor: "rgb(41,34,108)",
    marginVertical: 10
  }
});
