import React, { Fragment, Component } from "react";
import {
  View,
  Text,
  ScrollView,
  ActivityIndicator,
  TouchableOpacity,
  StyleSheet
} from "react-native";
class GetRecommendation extends Component {
  render() {
    return (
      <ScrollView style={styles.container} showsVerticalScrollIndicator={false}>
        <View style={[styles.outersection, { marginTop: 10 }]}>
          <Text style={styles.titletext}>Recommendation Settings For </Text>
          <Text style={styles.subtext}>UPC 500 Max</Text>
        </View>
        <View style={{ flexDirection: "row", justifyContent: "space-between" }}>
          <View style={[styles.outersection, { width: "30%" }]}>
            <Text style={styles.titletext}>A-Heater</Text>
            <Text style={styles.subtext}>122</Text>
          </View>
          <View style={[styles.outersection, { width: "30%" }]}>
            <Text style={styles.titletext}>B-Heater </Text>
            <Text style={styles.subtext}>120</Text>
          </View>
          <View style={[styles.outersection, { width: "30%" }]}>
            <Text style={styles.titletext}>Hose Heat </Text>
            <Text style={styles.subtext}>125</Text>
          </View>
        </View>
        <View style={styles.outersection}>
          <Text style={styles.titletext}>Pressure Set</Text>
          <Text style={styles.subtext}>UPC 500 Max</Text>
        </View>
        <View style={styles.outersection}>
          <Text style={styles.titletext}>Notes </Text>
          <Text style={styles.subtext}>UPC 500 Max</Text>
        </View>
        <View style={styles.outersection}>
          <Text style={styles.titletext}>Expected Approx Yield Per Set</Text>
          <Text style={styles.subtext}>22,000 bf +1-</Text>
        </View>
        <View style={styles.outersection}>
          <Text style={styles.titletext}>Expected Approx Core Density</Text>
          <Text style={styles.subtext}>.47 lb. ft3</Text>
        </View>
        <View style={styles.outersection}>
          <Text style={styles.titletext}>22,000 bf +1-</Text>
          <Text style={styles.subtext}>UPC 500 Max</Text>
        </View>
        <TouchableOpacity onPress={() => {}} style={styles.button}>
          <Text
            style={{
              color: "white",
              textAlign: "center",
              fontSize: 15,
              fontFamily: "Comfortaa-Bold"
              //   marginVertical : 10
            }}
          >
            Ask in Community
          </Text>
        </TouchableOpacity>
      </ScrollView>
    );
  }
}
export default GetRecommendation;
const styles = StyleSheet.create({
  outersection: {
    width: "100%",
    marginBottom: 10,
    backgroundColor: "rgb(233 ,232 ,240)",
    // height : 60,
    alignItems: "center",
    justifyContent: "center",
    paddingHorizontal: 10,
    paddingVertical: 10,
    borderRadius: 6
  },
  titletext: {
    fontSize: 12,
    color: "rgb(102,102,102)",
    fontFamily: "Comfortaa-Regular"
  },
  subtext: {
    fontSize: 14,
    color: "rgb(51 ,51 ,51)",
    fontFamily : 'Comfortaa-Bold'
  },
  button: {
    height: 47,
    width: "100%",
    borderRadius: 8,
    justifyContent: "center",
    alignSelf: "center",
    // alignItems: 'center',
    backgroundColor: "rgb(41,34,108)",
    marginBottom: 10
  }
});
