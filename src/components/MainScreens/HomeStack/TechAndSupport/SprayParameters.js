import React from "react";
import {
  View,
  Text,
  ScrollView,
  Image,
  Platform,
  StyleSheet,
  TouchableOpacity,
  ActivityIndicator
} from "react-native";
import RNPickerSelect from "react-native-picker-select";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp
} from "react-native-responsive-screen";
import QuestionPicker from "./QuestionPicker";
import { connect } from "react-redux";
import { TechSupportApi } from "../../../../config/api";
import axios from "axios";
import { setBasicSettings } from "../../../../Actions/sprayparameters";

class SprayParameters extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      general_question: [],
      advance_question: []
    };
  }
  
  onPickerValueChange = value => {
    this.props.setBasicSettings(value);
  };

  componentDidMount() {
    axios(TechSupportApi.getSprayParametersQuestion(this.props.accessToken))
      .then(res => {
        // console.log('question response......',res);
        if (res.status == 200) {
          const chuck = 5;
          this.setState({
            general_question: res.data.data.slice(0, chuck),
            advance_question: res.data.data.slice(chuck, res.data.data.length)
          });
        } else {
          alert("Server Error");
        }
      })
      .catch(err => {
        console.log("question response err", err);
      });
  }

  render() {
    return (
      <ScrollView
        showsVerticalScrollIndicator={false}
        contentContainerStyle={{
          paddingBottom: 20
        }}
      >
        <Text style={styles.titleStyle}>Basic Settings</Text>

        {this.state.general_question.length > 0 ? (
          this.state.general_question.map(item => (
            <QuestionPicker
              onValueChange={data => {
                this.onPickerValueChange(data);
              }}
              data={item}
            />
          ))
        ) : (
          <ActivityIndicator
            size={"large"}
            color={"rgb(41,34,108)"}
            style={{ marginTop: "auto", marginBottom: "auto" }}
          />
        )}

        <TouchableOpacity
          onPress={() =>
            this.props.navigation.navigate("AdvanceQuestion", {
              data: this.state.advance_question
            })
          }
          style={styles.button}
        >
          <Text
            style={{
              color: "white",
              textAlign: "center",
              fontSize: 15,
              fontFamily: "Comfortaa-Bold"
              //   marginVertical : 10
            }}
          >
            Advanced Settings
          </Text>
        </TouchableOpacity>
        <TouchableOpacity
          onPress={() =>
            // console.log("recommended data....", this.props.basicQuestions
            this.props.navigation.navigate('GetRecommendation')
          }
          style={[
            styles.button,
            {
              backgroundColor: "white",
              borderWidth: 1,
              borderColor: "rgb(41,34,108)",
              marginVertical: 0,
              marginBottom: 10
            }
          ]}
        >
          <Text
            style={{
              textAlign: "center",
              fontSize: 15,
              fontFamily: "Comfortaa-Bold",
              color: "rgb(41,34,108)"
            }}
          >
            Get Recomendation
          </Text>
        </TouchableOpacity>
      </ScrollView>
    );
  }
}
const mapStateToProps = ({ auth, TechSupport }) => {
  const { accessToken } = auth;
  const { basicQuestions } = TechSupport;
  return { accessToken, basicQuestions };
};
const actionCreater = {
  setBasicSettings
};

export default connect(mapStateToProps, actionCreater)(SprayParameters);
const styles = StyleSheet.create({
  titleStyle: {
    fontSize: 15,
    color: "rgb(51,51,51)",
    fontFamily: "Comfortaa-Bold",
    marginVertical: 10
  },
  question: {
    fontSize: 13,
    color: "rgb(51,51,51)",
    fontFamily: "Comfortaa-Bold",
    marginVertical: 10
  },
  viewContainer: {
    height: hp("5.5%"),
    width: wp("82%"),
    fontFamily: "Comfortaa-Bold",
    borderWidth: 0.6,
    marginLeft: "auto",
    marginRight: "auto",
    justifyContent: "center",
    borderColor: "rgba(200,200,200,1)",
    borderRadius: 5
  },
  button: {
    height: 47,
    width: "100%",
    borderRadius: 8,
    justifyContent: "center",
    alignSelf: "center",
    // alignItems: 'center',
    backgroundColor: "rgb(41,34,108)",
    marginVertical: 10
  }
});
