import React from "react";
import { Platform, Text } from "react-native";
import {
  createAppContainer,
  createMaterialTopTabNavigator,
  createStackNavigator,
  createSwitchNavigator
} from "react-navigation";
import Library from "./Library";
import Support from "./Support";
import i18n from "../../../../i18n";
import Faqs from "./Faqs";
import SprayParameters from "./SprayParameters";
import AdvanceQuestion from "./AdvanceQuestion";
import GetRecommendation from "./GetRecommendation";

const styles = {
  labelStyle: {
    fontSize: 12,
    fontFamily: "Comfortaa-Bold",
    textAlign: "center"
  }
};
const SprayParametersStack = createStackNavigator(
  {
    SprayParameters: SprayParameters,
    AdvanceQuestion: AdvanceQuestion,
    GetRecommendation: GetRecommendation
  },
  {
    headerMode: "none"
  }
);
const TechNavigator = createMaterialTopTabNavigator(
  {
    Support: {
      screen: Support,
      navigationOptions: ({ navigation }) => {
        const options = {
          tabBarLabel: (
            <Text
              style={{
                color: "rgb(41,34,108)",
                fontFamily: "Comfortaa-Bold",
                textTransform: "capitalize",
                textAlign: "center",
                fontSize: 12,
                width: "100%",
                marginTop: 20
              }}
            >
              {i18n.t("techSupport:support")}
            </Text>
          )
        };
        return options;
      }
    },
    Library: {
      screen: Library,
      navigationOptions: ({ navigation }) => {
        const options = {
          tabBarLabel: (
            <Text
              style={{
                color: "rgb(41,34,108)",
                fontFamily: "Comfortaa-Bold",
                textTransform: "capitalize",
                textAlign: "center",
                fontSize: 12,
                width: "100%",
                marginTop: 20
              }}
            >
              {i18n.t("techSupport:library")}
            </Text>
          )
        };
        return options;
      }
    },
    FAQs: {
      screen: Faqs,
      navigationOptions: ({ navigation }) => {
        const options = {
          tabBarLabel: (
            <Text
              style={{
                color: "rgb(41,34,108)",
                fontFamily: "Comfortaa-Bold",
                textTransform: "capitalize",
                textAlign: "center",
                fontSize: 12,
                width: "100%",
                marginTop: 20
              }}
            >
              FAQs
            </Text>
          )
        };
        return options;
      }
    },
    SprayParameters: {
      screen: SprayParametersStack,
      navigationOptions: ({ navigation }) => {
        const options = {
          tabBarLabel: "Spray Parameters"
        };
        return options;
      }
    }
  },
  {
    tabBarOptions: {
      activeTintColor: "rgb(41,34,108)",
      inactiveTintColor: "rgba(41,34,70,0.26)",
      indicatorStyle: {
        backgroundColor: "rgb(41,34,108)"
      },
      style: {
        backgroundColor: "white",
        elevation: 0,
        borderBottomWidth: 0.6,
        borderBottomColor: "rgb(41,34,108)",
        height: Platform.OS == "ios" ? "7%" : "12%"
      },
      tabStyle: {
        // justifyContent : 'flex-end',
      },
      labelStyle: {
        color: "rgb(41,34,108)",
        fontFamily: "Comfortaa-Bold",
        textTransform: "capitalize",
        textAlign: "center",
        fontSize: 12,
        width: "100%"
      }
    }
  }
);

const TechStackNavigator = createAppContainer(TechNavigator);

export default TechStackNavigator;
