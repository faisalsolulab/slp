import React, { Fragment } from "react";
import { View, Text, StyleSheet, Image, Platform } from "react-native";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp
} from "react-native-responsive-screen";
import RNPickerSelect from "react-native-picker-select";
import { Images } from "../../../../assets/images";
class QuestionPicker extends React.Component {
  render() {
    const { question, options, qid } = this.props.data;
    // console.log("questuib data", this.props.data);
    return (
      <Fragment key={qid.toString()}>
        <Text style={styles.question}>{question}</Text>
        <RNPickerSelect
          // onValueChange={value => this.props.onValueChange(value)}
          onValueChange={value => {
            const selecteddata = {
              qid: qid,
              question: question,
              selected_option: value
            };
            this.props.onValueChange(selecteddata);
          }}
          items={options.map(item => {
            return {
              label: item.toString(),
              value: item,
              color: "rgb(51,51,51)"
            };
          })}
          Icon={() => {
            return (
              <Image
                source={Images.downArrow}
                resizeMode="contain"
                style={{
                  height: hp("1%"),
                  top: Platform.OS == "ios" ? 5 : 0
                }}
              />
            );
          }}
          style={{
            fontFamily: "Comfortaa-Bold",
            inputIOS: {
              color: "rgb(51,51,51)",
              paddingLeft: 10
            },
            viewContainer: styles.viewContainer
          }}
        />
      </Fragment>
    );
  }
}
export default QuestionPicker;
const styles = StyleSheet.create({
  titleStyle: {
    fontSize: 15,
    color: "rgb(51,51,51)",
    fontFamily: "Comfortaa-Bold",
    marginVertical: 10
  },
  question: {
    fontSize: 13,
    color: "rgb(51,51,51)",
    fontFamily: "Comfortaa-Bold",
    marginVertical: 10
  },
  viewContainer: {
    height: hp("5.5%"),
    width: wp("82%"),
    fontFamily: "Comfortaa-Bold",
    borderWidth: 0.6,
    marginLeft: "auto",
    marginRight: "auto",
    justifyContent: "center",
    borderColor: "rgba(200,200,200,1)",
    borderRadius: 5
  },
  button: {
    height: 47,
    width: "100%",
    borderRadius: 8,
    justifyContent: "center",
    alignSelf: "center",
    // alignItems: 'center',
    backgroundColor: "rgb(41,34,108)",
    marginVertical: 10
  }
});
