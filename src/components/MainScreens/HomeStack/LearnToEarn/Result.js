import React from "react";
import {
  Text,
  View,
  ImageBackground,
  StyleSheet,
  FlatList,
  ActivityIndicator,
  TouchableOpacity
} from "react-native";
import { Images } from "../../../../assets/images";
import { connect } from "react-redux";
import RadioButton from "react-native-radio-button";
import axios from "axios";
import { LearnToEarnAPI, BASE_URL } from "../../../../config/api";
import { NavigationService } from "../../../../navigator";

class Result extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      quizData: "",
      selectedAnswers:'',
      selectedId: "",
      answerList: [],
      errorModal: false,
      spinner: false,
      message: "",
      result: false
    };
  }

  async componentDidMount() {
    if (this.props.accessToken) {
      axios
        .get(
          BASE_URL +
            "quiz_question/" +
            this.props.navigation.state.params.id +
            "?access_token=" +
            this.props.accessToken
        )
        .then(res => {
          console.log("quizData", res);
          this.setState({ quizData: res.data.data });
        });

      axios
        .get(
          BASE_URL + "quiz_show_result/" + this.props.navigation.state.params.id + "?access_token="+this.props.accessToken
        )
        .then(res => {
            console.log("RESPONSE", res.data)
            this.setState({selectedAnswers:res.data.data[0].user_Ans})
        });
    }
  }

  render() {
      const {selectedAnswers} = this.state;
    return (
      <ImageBackground
        source={Images.loginLayer}
        style={{ flex: 1, alignItems: "center" }}
      >
        <View style={styles.cardStyle}>
          {this.state.quizData && selectedAnswers? (
            <View style={{ flex: 1 }}>
              <FlatList
                data={this.state.quizData[0].questions}
                extraData={this.state}
                renderItem={({ item, index }) => {
                  index = index + 1;
                  let selectedAnswersID = selectedAnswers.map(item =>{return{...item, ID:index}})
                  let selectedAnswer = selectedAnswersID.filter(itemS => itemS.question_id === item.question_id)
                  return (
                    <View style={{ flex: 1, margin: "2%" }}>
                      <Text style={styles.questionStyle}>
                        Q.{index} {item.question}
                      </Text>
                      <View
                        style={{
                          flexDirection: "row",
                          justifyContent: "space-between"
                        }}
                      >
                        <View style={styles.answerContainer}>
                          <RadioButton
                            animation={"bounceIn"}
                            isSelected={selectedAnswer[0].answer == item.op1 || item.answer == item.op1?true:false}
                            innerColor={selectedAnswer[0].answer == item.op1 || item.answer == item.op1 ? item.op1 == item.answer ? "#14cb04" : "#c50301" : "rgb(41,34,108)"}
                            outerColor={selectedAnswer[0].answer == item.op1 || item.answer == item.op1 ? item.op1 == item.answer ? "#14cb04" : "#c50301" : "rgb(41,34,108)"}
                            size={10}
                          />
                          <View style={{ margin: "2%" }} />
                          <Text style={[styles.answerStyle,{color:selectedAnswer[0].answer == item.op1 || item.answer == item.op1 ? item.op1 == item.answer ? "#14cb04" : "#c50301" : "rgb(41,34,108)"}]}>{item.op1}</Text>
                        </View>
                        <View style={styles.answerContainer}>
                          <RadioButton
                            animation={"bounceIn"}
                            isSelected={selectedAnswer[0].answer == item.op2 || item.answer == item.op2?true:false}
                            innerColor={selectedAnswer[0].answer == item.op2 || item.answer == item.op2 ? item.op2 == item.answer ? "#14cb04" : "#c50301" : "rgb(41,34,108)"}
                            outerColor={selectedAnswer[0].answer == item.op2 || item.answer == item.op2 ? item.op2 == item.answer ? "#14cb04" : "#c50301" : "rgb(41,34,108)"}
                            size={10}
                          />
                          <View style={{ margin: "2%" }} />
                          <Text style={[styles.answerStyle,{color:selectedAnswer[0].answer == item.op2 || item.answer == item.op2 ? item.op2 == item.answer ? "#14cb04" : "#c50301" : "rgb(41,34,108)"}]}>{item.op2}</Text>
                        </View>
                      </View>
                      <View
                        style={{
                          flexDirection: "row",
                          justifyContent: "space-between"
                        }}
                      >
                        <View style={styles.answerContainer}>
                          <RadioButton
                            animation={"bounceIn"}
                            isSelected={selectedAnswer[0].answer == item.op3 || item.answer == item.op3?true:false}
                            innerColor={selectedAnswer[0].answer == item.op3 || item.answer == item.op3 ? item.op3 == item.answer ? "#14cb04" : "#c50301" : "rgb(41,34,108)"}
                            outerColor={selectedAnswer[0].answer == item.op3 || item.answer == item.op3 ? item.op3 == item.answer ? "#14cb04" : "#c50301" : "rgb(41,34,108)"}
                            size={10}
                          />
                          <View style={{ margin: "2%" }} />
                          <Text style={[styles.answerStyle,{color:selectedAnswer[0].answer == item.op3 || item.answer == item.op3 ? item.op3 == item.answer ? "#14cb04" : "#c50301" : "rgb(41,34,108)"}]}>{item.op3}</Text>
                        </View>
                        <View style={styles.answerContainer}>
                          <RadioButton
                            animation={"bounceIn"}
                            isSelected={selectedAnswer[0].answer == item.op4 || item.answer == item.op4?true:false}
                            innerColor={selectedAnswer[0].answer == item.op4 || item.answer == item.op4 ? item.op4 == item.answer ? "#14cb04" : "#c50301" : "rgb(41,34,108)"}
                            outerColor={selectedAnswer[0].answer == item.op4 || item.answer == item.op4 ? item.op4 == item.answer ? "#14cb04" : "#c50301" : "rgb(41,34,108)"}
                            size={10}
                          />
                          <View style={{ margin: "2%" }} />
                          <Text style={[styles.answerStyle,{color:selectedAnswer[0].answer == item.op4 || item.answer == item.op4 ? item.op4 == item.answer ? "#14cb04" : "#c50301" : "rgb(41,34,108)"}]}>{item.op4}</Text>
                        </View>
                      </View>
                      <View
                        style={{
                          borderWidth: 0.6,
                          opacity: 0.2,
                          marginTop: "4%"
                        }}
                      />
                    </View>
                  );
                }}
                keyExtractor={(item, index) => item.question_id}
                showsVerticalScrollIndicator={false}
              />
              <TouchableOpacity
                style={styles.button}
                onPress={() =>  NavigationService.navigate('LearnToEarn')}
              >
                <Text
                  style={{
                    color: "white",
                    textAlign: "center",
                    fontSize: 17,
                    fontFamily: "Comfortaa-Bold"
                  }}
                >
                  OK
                </Text>
              </TouchableOpacity>
            </View>
          ) : (
            <View
              style={{
                flex: 1,
                alignItems: "center",
                justifyContent: "center"
              }}
            >
              <ActivityIndicator size="large" />
            </View>
          )}
        </View>
      </ImageBackground>
    );
  }
}

const styles = StyleSheet.create({
  cardStyle: {
    height: "97%",
    width: "90%",
    padding: "2%",
    paddingBottom: 0,
    paddingTop: 10,
    elevation: 5,
    backgroundColor: "white",
    borderRadius: 10,
    shadowColor: "#000",
    shadowOffset: { width: 0, height: 1 },
    shadowOpacity: 0.5,
    shadowRadius: 2
  },
  questionStyle: {
    color: "black",
    fontFamily: "Comfortaa-Bold"
  },
  answerContainer: {
    flexDirection: "row",
    margin: "1%",
    marginRight: "auto",
    alignItems: "center"
  },
  answerStyle: {
    fontFamily: "Comfortaa-Bold",
    color: "rgb(102,102,102)",
    width: 120
  },
  button: {
    height: 47,
    width: 295,
    borderRadius: 8,
    justifyContent: "center",
    alignItems: "center",
    alignSelf: "center",
    backgroundColor: "rgb(41,34,108)",
    marginBottom: 10
  }
});

const mapStateToProps = ({ auth }) => {
  const { accessToken } = auth;
  return { accessToken };
};

export default connect(
  mapStateToProps,
  null
)(Result);
