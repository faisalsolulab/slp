import React from "react";
import {
  Text,
  View,
  StyleSheet,
  TouchableOpacity,
  TextInput,
  ImageBackground,
  Platform,
  Image,
  ScrollView
} from "react-native";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp
} from "react-native-responsive-screen";
import Icon from "react-native-vector-icons/Ionicons";
import ImagePicker from "react-native-image-picker";
import Permissions from "react-native-permissions";
import { connect } from "react-redux";
import Spinner from "react-native-spinkit";
import SnackBar from "react-native-snackbar-component";
import InfoModal from "./InfoModal";
import Modal from "@kalwani/react-native-modal";
import {
  activateProduct,
  hideModal,
  dontSplitAction,
  splitGetUserAction,
  cancelQrRequest
} from "../../../../Actions";
import { Images } from "../../../../assets/images";
import RNPickerSelect from "react-native-picker-select";
import i18n from "../../../../i18n/";
import { NavigationService } from "../../../../navigator";

class ProductActivation extends React.Component {
  constructor(props) {
    super(props);
    const paramdata = props.navigation.state.params;
    const qrdata = paramdata === undefined ? null : paramdata.qrdata;
    let starting_drum_temp;
    let a_side_set_temp;
    let b_side_set_temp;
    let hose_set_temp;
    let pressure_set;
    let mixing_chamber_size;
    if (qrdata) {
      starting_drum_temp = qrdata.start_drum_temp
        ? qrdata.start_drum_temp.toString()
        : null;
      a_side_set_temp = qrdata.a_side_set_temp
        ? qrdata.a_side_set_temp.toString()
        : null;
      b_side_set_temp = qrdata.b_side_set_temp
        ? qrdata.b_side_set_temp.toString()
        : null;
      hose_set_temp = qrdata.hose_set_temp
        ? qrdata.hose_set_temp.toString()
        : null;
      pressure_set = qrdata.pressure_set
        ? qrdata.pressure_set.toString()
        : null;
      mixing_chamber_size = qrdata.mixing_chamber_size
        ? qrdata.mixing_chamber_size.toString()
        : null;
    } else {
      starting_drum_temp = undefined;
      a_side_set_temp = undefined;
      b_side_set_temp = undefined;
      hose_set_temp = undefined;
      pressure_set = undefined;
      mixing_chamber_size = undefined;
    }

    this.state = {
      avatarSource: qrdata ? qrdata.image : null,
      photoPermission: "",
      isVisibleSubmit: false,
      isVisibleDontSplit: false,
      info: false,
      photoData: qrdata ? qrdata.image : null,
      submitModal: false,
      dontSplitModal: false,
      hundredfrequencylist: [],
      pressure_set_list: [],
      starting_drum_temp_list: [],
      // values of input
      a_side_batch: qrdata ? qrdata.a_side_batch : "",
      starting_drum_temp,
      a_side_set_temp,
      b_side_set_temp,
      hose_set_temp,
      pressure_set,
      mixing_chamber_size,
      user_rewards_id: qrdata ? qrdata.user_rewards_id.toString() : null,
      toggle_snackbar: false,
      snackbar_msg: "",
      selected_info_btn: "",
      isPhotoSelected: false
    };
  }

  _requestPermission = () => {
    Permissions.request("photo").then(response => {
      this.setState({ photoPermission: response });
    });
  };

  imagePicker = () => {
    const options = {
      title: "Select/Capture Image",
      storageOptions: {
        skipBackup: true,
        path: "images"
      },
      quality:0.5
    };

    ImagePicker.launchCamera(options, response => {
      if (response.didCancel) {
        console.log("User cancelled video picker");
      } else if (response.error) {
        console.log("ImagePicker Error: ", response.error);
      } else {
        console.log("photo response", response);
        this.setState({
          avatarSource: response.uri,
          photoData: response,
          isPhotoSelected: true
        });
      }
    });
  };

  spinner = loaderState => (
    <View>
      <Modal
        onModalHide={() => {
          if (this.props.dontSplitSuccess) {
            this.setState({ dontSplitModal: !this.state.dontSplitModal });
          }
          if (this.props.saveContinueSucess) {
            const paramdata = this.props.navigation.state.params;
            const qrdata = paramdata === undefined ? null : paramdata.qrdata;
            if (qrdata) {
              this.props.navigation.popToTop();
            } else {
              NavigationService.navigate("HomeScreen");
              NavigationService.navigate("QRHistory");
            }
          }
        }}
        hideModalContentWhileAnimating
        animationIn={"flash"}
        animationOut={"flash"}
        isVisible={loaderState}
        style={styles.modalStyle}
      >
        <Spinner
          isVisible={loaderState}
          size={50}
          type={"Circle"}
          color={"white"}
        />
        {this.props.uploadProgress ? (
          <Text
            style={{
              fontSize: 20,
              color: "white",
              marginTop: Platform.OS == "ios" ? 10 : 0,
              marginLeft: Platform.OS == "ios" ? 15 : 0
            }}
          >
            {this.props.uploadProgress} %
          </Text>
        ) : null}
      </Modal>
    </View>
  );
  showtoast = msg => {
    this.setState({
      toggle_snackbar: true,
      snackbar_msg: msg
    });
  };
  validateInput = () => {
    const {
      a_side_batch,
      starting_drum_temp,
      a_side_set_temp,
      b_side_set_temp,
      hose_set_temp,
      pressure_set,
      mixing_chamber_size,
      photoData
    } = this.state;
    if (a_side_batch === null || a_side_batch === "") {
      this.showtoast(i18n.t("productActivation:aBatchEmpty"));
    } else if (
      starting_drum_temp === null ||
      starting_drum_temp === undefined
    ) {
      this.showtoast(i18n.t("productActivation:drumTempEmpty"));
    } else if (a_side_set_temp === null || a_side_set_temp === undefined) {
      this.showtoast(i18n.t("productActivation:aSideEmpty"));
    } else if (b_side_set_temp === null || b_side_set_temp === undefined) {
      this.showtoast(i18n.t("productActivation:bSideEmpty"));
    } else if (hose_set_temp === null || hose_set_temp === undefined) {
      this.showtoast(i18n.t("productActivation:hoseEmpty"));
    } else if (pressure_set === null || pressure_set === undefined) {
      this.showtoast(i18n.t("productActivation:pressureEmpty"));
    } else if (
      mixing_chamber_size === null ||
      mixing_chamber_size === undefined
    ) {
      this.showtoast(i18n.t("productActivation:mixingEmpty"));
    } else if (photoData === null || photoData === undefined) {
      this.showtoast(i18n.t("productActivation:photoEmpty"));
    } else {
      return true;
    }
  };

  toggleModal = (text, selected_info_btn) => {
    selected_info_btn = selected_info_btn || "";
    const paramdata = this.props.navigation.state.params;
    const qrdata = paramdata === undefined ? null : paramdata.qrdata;
    const productid = qrdata
      ? qrdata.product_id
      : this.props.barrelData.product_id;
    const qr_id = qrdata ? qrdata.qr_id : this.props.barrelData.qr_id;

    if (text == "Cancel") {
      const token = this.props.accessToken;
      if (qrdata) {
        this.props.cancelQrRequest(
          token,
          qr_id,
          "QrHistory",
          this.props.navigation
        );
      } else {
        this.props.cancelQrRequest(
          token,
          qr_id,
          "ScanScreen",
          this.props.navigation
        );
      }
    }
    if (text == "SaveNContinue") {
      this.props.activateProduct(
        this.props.accessToken,
        productid,
        "true",
        qr_id,
        this.state.a_side_batch,
        this.state.starting_drum_temp,
        this.state.a_side_set_temp,
        this.state.b_side_set_temp,
        this.state.hose_set_temp,
        this.state.pressure_set,
        this.state.mixing_chamber_size,
        this.state.photoData,
        this.props.navigation,
        this.state.isPhotoSelected
      );
      this.setState({ isVisibleSubmit: !this.state.isVisibleSubmit });
    }
    if (text == "Submit") {
      let validate_input = this.validateInput();
      if (validate_input === true) {
        this.setState({ submitModal: !this.state.submitModal });
      }
    }
    if (text == "info") {
      this.setState({
        info: !this.state.info,
        selected_info_btn
      });
    }
  };

  renderSubmitActivation = () => {
    const paramdata = this.props.navigation.state.params;
    const qrdata = paramdata === undefined ? null : paramdata.qrdata;
    const productid = qrdata
      ? qrdata.product_id
      : this.props.barrelData.product_id;
    const qr_id = qrdata ? qrdata.qr_id : this.props.barrelData.qr_id;
    const screen = qrdata ? "QrHistory" : "ScanScreen";
    const is_photo_seleted = this.state.isPhotoSelected;
    return (
      <Modal
        // hideModalContentWhileAnimating
        onModalHide={() => {
          this.spinner(this.props.productActivationLoader);
        }}
        onBackButtonPress={() => {
          this.setState({ submitModal: !this.state.submitModal });
        }}
        onBackdropPress={() => {
          this.setState({ submitModal: !this.state.submitModal });
        }}
        animationIn={"bounceIn"}
        animationOut={"bounceOut"}
        animationInTiming={200}
        animationOutTiming={200}
        isVisible={this.state.submitModal}
        style={styles.modalStyle}
      >
        <View style={[styles.cardStyle, { height: 293 }]}>
          <View style={{ margin: hp("0.4%") }} />
          <Image
            source={Images.success}
            resizeMode="contain"
            style={{
              height: 50,
              alignSelf: "center"
            }}
          />
          <Text
            style={{
              textAlign: "center",
              fontFamily: "Comfortaa-Bold",
              color: "rgb(51,51,51)",
              margin: "5%",
              lineHeight: 24
            }}
          >
            {i18n.t("productActivation:modalMessage")}
          </Text>
          <TouchableOpacity
            style={[
              styles.buttonModal,
              { height: 45, alignSelf: "center", alignItems: "center" }
            ]}
            onPress={() => {
              this.setState({ submitModal: !this.state.submitModal });
              console.log("user_Reward id", this.state.user_rewards_id);
              this.props.splitGetUserAction(
                this.props.accessToken,
                this.props.navigation,
                productid,
                false,
                qr_id,
                this.state.a_side_batch,
                this.state.starting_drum_temp,
                this.state.a_side_set_temp,
                this.state.b_side_set_temp,
                this.state.hose_set_temp,
                this.state.pressure_set,
                this.state.mixing_chamber_size,
                this.state.photoData,
                screen,
                this.state.user_rewards_id,
                is_photo_seleted
              );
              // this.props.navigation.navigate("SplitScreen");
            }}
          >
            <Text
              style={{
                color: "white",
                textAlign: "center",
                fontSize: 16,
                fontFamily: "Comfortaa-Bold"
              }}
            >
              {i18n.t("productActivation:split")}
            </Text>
          </TouchableOpacity>
          <View style={{ margin: "2%" }} />
          <TouchableOpacity
            style={[
              styles.buttonModal,
              {
                height: 45,
                backgroundColor: "white",
                borderWidth: 1,
                borderColor: "rgb(41,34,108)",
                alignSelf: "center"
              }
            ]}
            onPress={() => {
              // this.props.hideModal();
              this.setState({ submitModal: !this.state.submitModal });
              this.props.dontSplitAction(
                this.props.accessToken,
                productid,
                false,
                qr_id,
                this.state.a_side_batch,
                this.state.starting_drum_temp,
                this.state.a_side_set_temp,
                this.state.b_side_set_temp,
                this.state.hose_set_temp,
                this.state.pressure_set,
                this.state.mixing_chamber_size,
                this.state.photoData,
                this.props.navigation,
                this.state.user_rewards_id,
                is_photo_seleted
              );
              // this.toggleModal("DontSplit");
            }}
          >
            <Text
              style={{
                color: "rgb(41,34,108)",
                textAlign: "center",
                fontSize: 16,
                fontFamily: "Comfortaa-Bold"
              }}
            >
              {i18n.t("productActivation:dontSplit")}
            </Text>
          </TouchableOpacity>
        </View>
      </Modal>
    );
  };

  renderDontSplit = () => {
    return (
      <Modal
        onModalHide={() => {}}
        animationIn={"bounceIn"}
        animationOut={"bounceOut"}
        animationInTiming={200}
        animationOutTiming={200}
        isVisible={this.state.dontSplitModal}
        style={styles.modalStyle}
      >
        <View style={[styles.cardStyle, { height: 260 }]}>
          <View style={{ margin: hp("0.4%") }} />
          <Image
            source={Images.success}
            resizeMode="contain"
            style={{
              height: 50,
              alignSelf: "center"
            }}
          />
          <Text
            style={{
              textAlign: "center",
              fontFamily: "Comfortaa-Bold",
              color: "rgb(51,51,51)",
              margin: "5%",
              lineHeight: 24
            }}
          >
            {i18n.t("productActivation:sucessMessage")}
          </Text>
          <TouchableOpacity
            style={[styles.buttonModal, { height: 45, alignSelf: "center" }]}
            onPress={() => {
              this.setState({ dontSplitModal: !this.state.dontSplitModal });
              setTimeout(() => {
                const paramdata = this.props.navigation.state.params;
                const qrdata =
                  paramdata === undefined ? null : paramdata.qrdata;
                if (qrdata) {
                  this.props.navigation.popToTop();
                  NavigationService.navigate("RewardPoints");
                } else {
                  NavigationService.navigate("HomeScreen");
                  NavigationService.navigate("RewardPoints");
                }
              }, 500);
            }}
          >
            <Text
              style={{
                color: "white",
                textAlign: "center",
                fontSize: 16,
                fontFamily: "Comfortaa-Bold"
              }}
            >
              OK
            </Text>
          </TouchableOpacity>
        </View>
      </Modal>
    );
  };

  renderInfo = () => {
    return (
      <Modal
        onModalHide={() => {}}
        // hideModalContentWhileAnimating
        animationIn={"bounceIn"}
        animationOut={"bounceOut"}
        animationInTiming={200}
        animationOutTiming={200}
        isVisible={this.state.info}
        style={styles.modalStyle}
      >
        <View
          style={[
            styles.cardStyle,
            {
              height: "auto",
              justifyContent: "center",
              alignItems: "center",
              paddingTop: "2%"
            }
          ]}
        >
          <InfoModal infobtntext={this.state.selected_info_btn} />
          <TouchableOpacity
            style={[
              styles.buttonModal,
              { height: 47, alignSelf: "center", marginBottom: "2%" }
            ]}
            onPress={() => {
              this.toggleModal("info");
            }}
          >
            <Text
              style={{
                color: "white",
                textAlign: "center",
                fontSize: 16,
                fontFamily: "Comfortaa-Bold",
                marginBottom: Platform.OS == "android" ? "2%" : 0
              }}
            >
              Ok
            </Text>
          </TouchableOpacity>
        </View>
      </Modal>
    );
  };
  componentDidMount = () => {
    let tempvalue = [];
    let tempvalue2 = [];
    let tempvalue3 = [];
    for (let i = 100; i <= 150; i++) {
      if (i % 2 === 0) {
        tempvalue.push(i);
      }
    }
    for (let j = 900; j <= 1600; j++) {
      if (j % 25 === 0) {
        tempvalue2.push(j);
      }
    }
    for (let k = 40; k <= 100; k++) {
      if (k % 2 === 0) {
        tempvalue3.push(k);
      }
    }
    this.setState({
      hundredfrequencylist: tempvalue,
      pressure_set_list: tempvalue2,
      starting_drum_temp_list: tempvalue3
    });
    // console.log('data', this.state.hundredfrequencylist)
  };
  changeText = text => {
    this.setState({ a_side_batch: text });
  };
  changeDropDownValue = (value, text) => {
    if (value === undefined) {
      this.setState({
        [text]: value
      });
    } else {
      console.log("change", value, text);
      this.setState(
        {
          [text]: value ? value.toString() : null
        },
        () => {
          console.log(this.state.a_side_set_temp);
        }
      );
    }
    // this.setState({
    //   a_side_batch: value
    // })
  };
  render() {
    console.log("STATE", this.state);
    return (
      <View style={{ flex: 1 }}>
        {this.renderSubmitActivation()}
        {this.renderDontSplit()}
        {this.renderInfo()}
        {this.spinner(this.props.productActivationLoader)}
        <ImageBackground
          style={{ flex: 1, alignItems: "center" }}
          source={require("../../../../assets/loginLayer.jpg")}
        >
          <View style={styles.cardStyle}>
            <ScrollView
              style={{ flex: 1 }}
              showsVerticalScrollIndicator={false}
            >
              <View style={{ flex: 1 }}>
                <Text style={styles.titleStyle}>
                  {i18n.t("productActivation:pleaseFillOutDetails")}
                </Text>
                <View style={{ flex: 2 }}>
                  <Text style={styles.titleStyle2}>
                    {i18n.t("productActivation:aBatch")}
                  </Text>
                  <View
                    style={{
                      flexDirection: "row",
                      alignItems: "center",
                      justifyContent: "space-between"
                    }}
                  >
                    <TextInput
                      placeholder={i18n.t("productActivation:enterABatch")}
                      placeholderTextColor="rgba(102,102,102,0.7)"
                      style={styles.textInputStyle}
                      onChangeText={text => this.changeText(text)}
                      value={this.state.a_side_batch}
                    />
                    <TouchableOpacity
                      onPress={() => this.toggleModal("info", "A_Side_Batch")}
                    >
                      <Image
                        source={Images.info}
                        style={styles.infoStyle}
                        resizeMode="contain"
                      />
                    </TouchableOpacity>
                  </View>
                  {/* added new input */}
                  <Text style={styles.titleStyle2}>
                    {i18n.t("productActivation:drumTemp")}
                  </Text>
                  <View
                    style={{
                      alignItems: "center",
                      flexDirection: "row",
                      justifyContent: "space-between"
                    }}
                  >
                    <RNPickerSelect
                      onValueChange={value =>
                        this.changeDropDownValue(value, "starting_drum_temp")
                      }
                      placeholder={{
                        label: i18n.t("productActivation:enterDrum")
                      }}
                      placeholderTextColor="rgba(102,102,102,0.7)"
                      style={{
                        inputIOS: {
                          color: "rgb(51,51,51)",
                          width: wp("68%")
                        },
                        viewContainer: {
                          flex: 1,
                          height: hp("5.5%"),
                          width: "50%",
                          fontFamily: "Comfortaa-Bold",
                          borderWidth: 0.6,
                          marginLeft: "auto",
                          marginRight: 5,
                          justifyContent: "center",
                          borderColor: "rgba(200,200,200,1)",
                          borderRadius: 5,
                          paddingLeft: Platform.OS == "ios" ? 8 : 2,
                          paddingRight: Platform.OS == "ios" ? 5 : 15,
                          fontSize: 13
                        }
                      }}
                      value={this.state.starting_drum_temp}
                      Icon={() => {
                        return (
                          <View style={{ marginLeft: 8 }}>
                            <Icon
                              name="ios-arrow-down"
                              type={"antdesign"}
                              style={{ marginRight: 5 }}
                              size={15}
                            />
                          </View>
                        );
                      }}
                      items={
                        this.state.starting_drum_temp_list.length > 0
                          ? this.state.starting_drum_temp_list.map(element => {
                              return {
                                label: element.toString(),
                                value: element.toString(),
                                color: "rgb(51,51,51)"
                              };
                            })
                          : [{ label: "", value: "" }]
                      }
                    />
                    <TouchableOpacity
                      onPress={() =>
                        this.toggleModal("info", "Starting_Drum_Temperature")
                      }
                    >
                      <Image
                        source={Images.info}
                        style={styles.infoStyle}
                        resizeMode="contain"
                      />
                    </TouchableOpacity>
                  </View>
                  <Text style={styles.titleStyle2}>
                    {i18n.t("productActivation:aSide")}
                  </Text>
                  <View
                    style={{
                      alignItems: "center",
                      flexDirection: "row",
                      justifyContent: "space-between"
                    }}
                  >
                    {/* {
                      qrdata ? this.setState({
                        a_side_set_temp : qrdata.a_side_set_temp.toString()
                      }) : undefined
                    } */}
                    <RNPickerSelect
                      onValueChange={value =>
                        this.changeDropDownValue(value, "a_side_set_temp")
                      }
                      placeholder={{
                        label: i18n.t("productActivation:enterASide")
                      }}
                      placeholderTextColor="rgba(102,102,102,0.7)"
                      style={{
                        inputIOS: {
                          color: "rgb(51,51,51)",
                          width: wp("68%")
                        },
                        viewContainer: {
                          flex: 1,
                          height: hp("5.5%"),
                          fontFamily: "Comfortaa-Bold",
                          borderWidth: 0.6,
                          marginLeft: "auto",
                          marginRight: 5,
                          justifyContent: "center",
                          borderColor: "rgba(200,200,200,1)",
                          borderRadius: 5,
                          paddingLeft: Platform.OS == "ios" ? 8 : 2,
                          paddingRight: Platform.OS == "ios" ? 5 : 15,
                          fontSize: 13,
                          color: "black"
                        }
                      }}
                      value={this.state.a_side_set_temp}
                      Icon={() => {
                        return (
                          <Icon
                            name="ios-arrow-down"
                            type={"antdesign"}
                            style={{ marginRight: 5 }}
                            size={15}
                          />
                        );
                      }}
                      // items={
                      //   [{label: 'asda', value: 'asdhas'}]
                      // }

                      items={
                        this.state.hundredfrequencylist.length > 0
                          ? this.state.hundredfrequencylist.map(element => {
                              return {
                                label: element.toString(),
                                value: element.toString(),
                                color: "rgb(51,51,51)"
                              };
                            })
                          : [{ label: "", value: "" }]
                      }
                    />
                    <TouchableOpacity
                      onPress={() =>
                        this.toggleModal("info", "A_Side_Set_Temperature")
                      }
                    >
                      <Image
                        source={Images.info}
                        style={styles.infoStyle}
                        resizeMode="contain"
                      />
                    </TouchableOpacity>
                  </View>
                  <Text style={styles.titleStyle2}>
                    {i18n.t("productActivation:bSide")}
                  </Text>
                  <View
                    style={{
                      flexDirection: "row",
                      alignItems: "center",
                      justifyContent: "space-between"
                    }}
                  >
                    <RNPickerSelect
                      onValueChange={value =>
                        this.changeDropDownValue(value, "b_side_set_temp")
                      }
                      placeholder={{
                        label: i18n.t("productActivation:enterBSide")
                      }}
                      placeholderTextColor="rgba(102,102,102,0.7)"
                      style={{
                        inputIOS: {
                          color: "rgb(51,51,51)",
                          width: wp("68%")
                        },
                        viewContainer: {
                          flex: 1,
                          height: hp("5.5%"),
                          fontFamily: "Comfortaa-Bold",
                          borderWidth: 0.6,
                          marginLeft: "auto",
                          marginRight: 5,
                          justifyContent: "center",
                          borderColor: "rgba(200,200,200,1)",
                          borderRadius: 5,
                          paddingRight: Platform.OS == "ios" ? 5 : 15,
                          paddingLeft: Platform.OS == "ios" ? 8 : 2,
                          fontSize: 13,
                          color: "black"
                        }
                      }}
                      value={this.state.b_side_set_temp}
                      Icon={() => {
                        return (
                          <Icon
                            name="ios-arrow-down"
                            type={"antdesign"}
                            style={{ marginRight: 5 }}
                            size={15}
                          />
                        );
                      }}
                      // items={[
                      //   { label: "AllFoam", value: "football" },
                      //   { label: "Sprayer", value: "baseball" },
                      //   { label: "Solulab", value: "hockey" },
                      // ]}
                      items={
                        this.state.hundredfrequencylist.length > 0
                          ? this.state.hundredfrequencylist.map(element => {
                              return {
                                label: element.toString(),
                                value: element.toString(),
                                color: "rgb(51,51,51)"
                              };
                            })
                          : [{ label: "", value: "" }]
                      }
                    />
                    <TouchableOpacity
                      onPress={() =>
                        this.toggleModal("info", "B_Side_Set_Temperature")
                      }
                    >
                      <Image
                        source={Images.info}
                        style={styles.infoStyle}
                        resizeMode="contain"
                      />
                    </TouchableOpacity>
                  </View>
                  <Text style={styles.titleStyle2}>
                    {i18n.t("productActivation:hose")}
                  </Text>
                  <View
                    style={{
                      flexDirection: "row",
                      alignItems: "center",
                      justifyContent: "space-between"
                    }}
                  >
                    <RNPickerSelect
                      onValueChange={value =>
                        this.changeDropDownValue(value, "hose_set_temp")
                      }
                      placeholder={{
                        label: i18n.t("productActivation:enterHose")
                      }}
                      placeholderTextColor="rgba(102,102,102,0.7)"
                      style={{
                        inputIOS: {
                          color: "rgb(51,51,51)",
                          width: wp("68%")
                        },
                        viewContainer: {
                          flex: 1,
                          height: hp("5.5%"),
                          fontFamily: "Comfortaa-Bold",
                          borderWidth: 0.6,
                          marginLeft: "auto",
                          marginRight: 5,
                          justifyContent: "center",
                          borderColor: "rgba(200,200,200,1)",
                          borderRadius: 5,
                          paddingLeft: Platform.OS == "ios" ? 8 : 2,
                          paddingRight: Platform.OS == "ios" ? 5 : 15,
                          fontSize: 13
                        }
                      }}
                      value={this.state.hose_set_temp}
                      Icon={() => {
                        return (
                          <Icon
                            name="ios-arrow-down"
                            type={"antdesign"}
                            style={{ marginRight: 5 }}
                            size={15}
                          />
                        );
                      }}
                      // items={[
                      //   { label: "AllFoam", value: "football" },
                      //   { label: "Sprayer", value: "baseball" },
                      //   { label: "Solulab", value: "hockey" },
                      // ]}
                      items={
                        this.state.hundredfrequencylist.length > 0
                          ? this.state.hundredfrequencylist.map(element => {
                              return {
                                label: element.toString(),
                                value: element.toString(),
                                color: "rgb(51,51,51)"
                              };
                            })
                          : [{ label: "", value: "" }]
                      }
                    />
                    <TouchableOpacity
                      onPress={() =>
                        this.toggleModal("info", "Hose_Set_Temperature")
                      }
                    >
                      <Image
                        source={Images.info}
                        style={styles.infoStyle}
                        resizeMode="contain"
                      />
                    </TouchableOpacity>
                  </View>
                  <Text style={styles.titleStyle2}>
                    {i18n.t("productActivation:pressure")}
                  </Text>
                  <View
                    style={{
                      flexDirection: "row",
                      alignItems: "center",
                      justifyContent: "space-between"
                    }}
                  >
                    <RNPickerSelect
                      onValueChange={value =>
                        this.changeDropDownValue(value, "pressure_set")
                      }
                      placeholder={{
                        label: i18n.t("productActivation:enterPressure")
                      }}
                      placeholderTextColor="rgba(102,102,102,0.7)"
                      style={{
                        inputIOS: {
                          color: "rgb(51,51,51)",
                          width: wp("68%")
                        },
                        viewContainer: {
                          flex: 1,
                          height: hp("5.5%"),
                          fontFamily: "Comfortaa-Bold",
                          borderWidth: 0.6,
                          marginLeft: "auto",
                          marginRight: 5,
                          justifyContent: "center",
                          borderColor: "rgba(200,200,200,1)",
                          borderRadius: 5,
                          paddingLeft: Platform.OS == "ios" ? 8 : 2,
                          paddingRight: Platform.OS == "ios" ? 5 : 15,
                          fontSize: 13
                        }
                      }}
                      value={this.state.pressure_set}
                      Icon={() => {
                        return (
                          <Icon
                            name="ios-arrow-down"
                            type={"antdesign"}
                            style={{ marginRight: 5 }}
                            size={15}
                          />
                        );
                      }}
                      items={
                        this.state.pressure_set_list.length > 0
                          ? this.state.pressure_set_list.map(element => {
                              return {
                                label: element.toString(),
                                value: element.toString(),
                                color: "rgb(51,51,51)"
                              };
                            })
                          : [{ label: "", value: "" }]
                      }
                    />
                    <TouchableOpacity
                      onPress={() =>
                        this.toggleModal("info", "Pressure_Set_Temperature")
                      }
                    >
                      <Image
                        source={Images.info}
                        style={styles.infoStyle}
                        resizeMode="contain"
                      />
                    </TouchableOpacity>
                  </View>
                  <Text style={styles.titleStyle2}>
                    {i18n.t("productActivation:mixing")}
                  </Text>
                  <View
                    style={{
                      flexDirection: "row",
                      alignItems: "center",
                      justifyContent: "space-between"
                    }}
                  >
                    <RNPickerSelect
                      onValueChange={value =>
                        this.changeDropDownValue(value, "mixing_chamber_size")
                      }
                      placeholder={{
                        label: i18n.t("productActivation:enterMixing")
                      }}
                      value={this.state.mixing_chamber_size}
                      placeholderTextColor="rgba(102,102,102,0.7)"
                      style={{
                        inputIOS: {
                          color: "rgb(51,51,51)",
                          width: wp("70%")
                        },
                        viewContainer: {
                          flex: 1,
                          height: hp("5.5%"),
                          fontFamily: "Comfortaa-Bold",
                          borderWidth: 0.6,
                          marginLeft: "auto",
                          marginRight: 5,
                          justifyContent: "center",
                          borderColor: "rgba(200,200,200,1)",
                          borderRadius: 5,
                          paddingRight: Platform.OS == "ios" ? 5 : 15,
                          paddingLeft: Platform.OS == "ios" ? 8 : 2
                        }
                      }}
                      Icon={() => {
                        return (
                          <Icon
                            name="ios-arrow-down"
                            type={"antdesign"}
                            style={{ marginRight: 5 }}
                            size={15}
                          />
                        );
                      }}
                      items={[
                        { label: "0", value: "0", color: "rgb(51,51,51)" },
                        { label: "1", value: "1", color: "rgb(51,51,51)" },
                        { label: "2", value: "2", color: "rgb(51,51,51)" },
                        { label: "3", value: "3", color: "rgb(51,51,51)" }
                      ]}
                    />
                    <TouchableOpacity
                      onPress={() =>
                        this.toggleModal("info", "Mixing_Chamber_Size")
                      }
                    >
                      <Image
                        source={Images.info}
                        style={styles.infoStyle}
                        resizeMode="contain"
                      />
                    </TouchableOpacity>
                  </View>
                </View>
                <View style={{ margin: hp("1%") }} />
                <View
                  style={{
                    flexDirection: "row",
                    alignItems: "center"
                  }}
                >
                  <Text style={[styles.titleStyle, { margin: 0 }]}>
                    {i18n.t("productActivation:photo")}
                  </Text>
                  <TouchableOpacity
                    onPress={this.imagePicker}
                    style={{ marginLeft: "auto" }}
                  >
                    <Image
                      source={Images.camera}
                      resizeMode='contain'
                      style={{ height: 35, width: 35 }}
                    />
                  </TouchableOpacity>
                </View>
                <View style={{ margin: "1%" }} />
                {this.state.avatarSource ? (
                  <Image
                    source={{ uri: this.state.avatarSource }}
                    style={styles.imageStyle}
                    resizeMode="cover"
                  />
                ) : (
                  <Image
                    source={Images.promotion}
                    style={styles.imageStyle}
                    resizeMode="cover"
                  />
                )}
                <View style={{ marginTop: "4%" }} />
                <TouchableOpacity
                  style={styles.button}
                  onPress={() => {
                    this.toggleModal("Submit");
                  }}
                >
                  <Text
                    style={{
                      color: "white",
                      textAlign: "center",
                      fontSize: 17,
                      fontFamily: "Comfortaa-Bold",
                      marginBottom: Platform.OS == "android" ? "2%" : 0
                    }}
                  >
                    {i18n.t("common:submit")}
                  </Text>
                </TouchableOpacity>
                {console.log("value..", this.props.navigation.state.params)}
                {!this.props.navigation.state.params ? (
                  <View>
                    <View style={{ margin: "2%" }} />
                    <TouchableOpacity
                      onPress={() => this.toggleModal("SaveNContinue")}
                      style={styles.button2}
                    >
                      <Text
                        style={{
                          color: "rgb(41,34,108)",
                          textAlign: "center",
                          fontSize: 17,
                          fontFamily: "Comfortaa-Bold",
                          marginBottom: Platform.OS == "android" ? "2%" : 0
                        }}
                      >
                        {i18n.t("productActivation:saveContinue")}
                      </Text>
                    </TouchableOpacity>
                  </View>
                ) : null}
                {!this.props.navigation.state.params ? (
                  <View>
                    <View style={{ margin: "2%" }} />

                    <TouchableOpacity
                      onPress={() => this.toggleModal("Cancel")}
                      style={styles.button2}
                    >
                      <Text
                        style={{
                          color: "rgb(41,34,108)",
                          textAlign: "center",
                          fontSize: 17,
                          fontFamily: "Comfortaa-Bold",
                          marginBottom: Platform.OS == "android" ? "2%" : 0
                        }}
                      >
                        {i18n.t("common:cancel")}
                      </Text>
                    </TouchableOpacity>
                  </View>
                ) : null}
                <View style={{ margin: "2%" }} />
              </View>
            </ScrollView>
            <SnackBar
              visible={this.state.toggle_snackbar}
              accentColor="white"
              backgroundColor="rgb(41,34,108)"
              position="bottom"
              textMessage={this.state.snackbar_msg}
              actionHandler={() => {
                this.setState({
                  toggle_snackbar: false,
                  snackbar_msg: ""
                });
              }}
              actionText="Ok"
            />
          </View>
        </ImageBackground>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  cardStyle: {
    height: "95%",
    width: "90%",
    padding: "4%",
    paddingBottom: 0,
    paddingTop: 10,
    elevation: 5,
    backgroundColor: "white",
    borderRadius: 10,
    shadowColor: "#000",
    shadowOffset: { width: 0, height: 1 },
    shadowOpacity: 0.5,
    shadowRadius: 2
  },
  buttonModal: {
    width: wp("60%"),
    borderRadius: 8,
    justifyContent: "center",
    backgroundColor: "rgb(41,34,108)"
  },
  titleStyle: {
    fontFamily: "Comfortaa-Bold",
    fontSize: 17,
    color: "rgb(51,51,51)",
    margin: 10,
    marginLeft: 0
  },
  titleStyle2: {
    fontFamily: "Comfortaa-Bold",
    fontSize: 15,
    color: "rgb(51,51,51)",
    marginVertical: 8,
    marginLeft: 2
  },
  imageStyle: {
    height: hp("17%"),
    width: wp("82%"),
    borderRadius: 10
  },
  button: {
    height: 45,
    width: "100%",
    borderRadius: 8,
    justifyContent: "center",
    backgroundColor: "rgb(41,34,108)"
  },
  button2: {
    height: 45,
    width: "100%",
    borderRadius: 8,
    borderWidth: 1,
    borderColor: "rgb(41,34,108)",
    justifyContent: "center",
    backgroundColor: "white"
  },
  textInputStyle: {
    height: hp("5.5%"),
    width: wp("75%"),
    padding: 0,
    color: "rgb(51,51,51)",
    paddingLeft: 10,
    borderWidth: 0.5,
    fontWeight: Platform.OS == 'ios' ?"300": "100",
    borderColor: "rgba(200,200,200,1)",
    borderRadius: 5,
    fontSize: 15
  },
  infoStyle: {
    height: hp("3%"),
    width: wp("5%"),
    marginLeft: "auto",
    marginRight: "auto"
  },
  modalStyle: {
    justifyContent: "center",
    alignItems: "center"
  }
});

const mapStateToProps = ({ barrel, auth }) => {
  const {
    barrelData,
    saveContinueSucess,
    submitModal,
    dontSplitModal,
    productActivationLoader,
    dontSplitSuccess,
    dontSpliterror,
    userListError,
    uploadProgress
  } = barrel;
  const { userData, accessToken, userId } = auth;
  return {
    accessToken,
    userId,
    barrelData,
    saveContinueSucess,
    userData,
    submitModal,
    dontSplitModal,
    productActivationLoader,
    dontSplitSuccess,
    dontSpliterror,
    userListError,
    uploadProgress
  };
};

const actionCreater = {
  activateProduct,
  hideModal,
  dontSplitAction,
  splitGetUserAction,
  cancelQrRequest
};

export default connect(mapStateToProps, actionCreater)(ProductActivation);
