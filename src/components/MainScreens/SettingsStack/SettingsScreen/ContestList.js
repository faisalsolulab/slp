import React from "react";
import {
  Text,
  View,
  TouchableOpacity,
  StyleSheet,
  ImageBackground,
  FlatList,
  Image,
  Dimensions,
  ActivityIndicator,
  Platform,
  RefreshControl
} from "react-native";
import { Images } from "../../../../assets/images";
import i18n from "../../../../i18n";
import axios from "axios";
import { contestAPI } from "../../../../config/api";
import { connect } from "react-redux";
import moment from "moment-timezone";
import { NavigationEvents } from "react-navigation";

class ContestList extends React.Component {
  constructor(props) {
    super(props);
    this.state = { data: "", refreshing: false };
  }

  getContestList = () => {
    axios
      .get(contestAPI.contestList(this.props.userId, this.props.accessToken), {
        headers: {
          "Accept-Language": i18n.language.includes("es") ? "es" : null
        }
      })
      .then(res => {
        this.setState({ data: res.data.data });
      });
  };

  render() {
    return (
      <ImageBackground
        source={Images.loginLayer}
        style={{ flex: 1, alignItems: "center" }}
      >
        <NavigationEvents
          onWillFocus={() => {
            this.getContestList();
          }}
        />
        <View style={styles.cardStyle}>
          {this.state.data.length > 0 ? (
            <FlatList
              data={this.state.data}
              extraData={this.state.data}
              showsVerticalScrollIndicator={false}
              onRefresh={() => this.getContestList()}
              refreshing={this.state.refreshing}
              renderItem={({ item }) => (
                <TouchableOpacity
                  style={styles.contestView}
                  onPress={() =>
                    this.props.navigation.navigate("Contest", {
                      contestDetails: item
                    })
                  }
                  activeOpacity={0.6}
                >
                  <Image
                    source={{ uri: item.contest_image }}
                    style={styles.contestImage}
                  />
                  <View style={{ marginLeft: 15 }}>
                    <Text
                      style={[
                        styles.title,
                        {
                          marginVertical: Platform.OS == "ios" ? 3 : 0,
                          flexWrap: "wrap",
                          width: "95%"
                        }
                      ]}
                      numberOfLines={1}
                    >
                      {item.contest_name}
                    </Text>
                    <View>
                      <Text
                        style={[
                          styles.title,
                          {
                            fontSize: 13,
                            marginBottom: Platform.OS == "ios" ? 3 : 0
                          }
                        ]}
                      >
                        {i18n.t("common:status")} :{" "}
                        <Text
                          style={{
                            color:
                              item.contest_status == "completed" ||
                              item.contest_status == "terminada"
                                ? "rgb(197,3,1)"
                                : item.contest_status == "upcoming" ||
                                  item.contest_status == "próxima"
                                ? "#d4af37"
                                : "rgb(20,203,4)",
                            textTransform: "capitalize"
                          }}
                        >
                          {item.contest_status}
                        </Text>
                      </Text>
                      <Text
                        style={[
                          styles.title,
                          {
                            color: "rgb(102,102,102)",
                            fontSize: 12,
                            width: Dimensions.get("window").width / 1.7
                          }
                        ]}
                        ellipsizeMode="tail"
                        numberOfLines={1}
                      >
                        {i18n.t("common:dateTime")} :{" "}
                        {moment
                          .tz(item.contest_startdate, "America/New_York")
                          .format("YYYY-MM-DD hh:mm A")}
                      </Text>
                    </View>
                  </View>
                </TouchableOpacity>
              )}
            />
          ) : !this.state.data ? (
            <View
              style={{
                justifyContent: "center",
                alignItems: "center",
                flex: 1
              }}
            >
              <ActivityIndicator size="large" />
            </View>
          ) : (
            <View
              style={{
                justifyContent: "center",
                alignItems: "center",
                flex: 1
              }}
            >
              <Text style={{ fontSize: 15, fontFamily: "Comfortaa-Bold" }}>
                No contests at the moment.
              </Text>
            </View>
          )}
        </View>
      </ImageBackground>
    );
  }
}
const styles = StyleSheet.create({
  cardStyle: {
    height: "95%",
    width: "90%",
    padding: "4%",
    paddingBottom: 0,
    paddingTop: 10,
    elevation: 5,
    backgroundColor: "white",
    borderRadius: 10,
    shadowColor: "#000",
    shadowOffset: { width: 0, height: 1 },
    shadowOpacity: 0.5,
    shadowRadius: 2
  },
  contestView: {
    width: Dimensions.get("window").width,
    flexDirection: "row",
    marginVertical: 5,
    borderBottomWidth: 0.6,
    paddingBottom: 10,
    borderBottomColor: "rgba(41,34,108,0.3)"
  },
  contestImage: {
    height: 66,
    width: 77,
    borderRadius: 8
  },
  title: {
    fontFamily: "Comfortaa-Bold",
    fontSize: 16,
    color: "rgb(51,51,51)"
  }
});

const mapStateToProps = ({ auth }) => {
  const { userId, accessToken } = auth;
  return { userId, accessToken };
};

export default connect(mapStateToProps, null)(ContestList);
