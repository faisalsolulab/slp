import React from "react";
import {
  Text,
  View,
  StyleSheet,
  ImageBackground,
  Image,
  TouchableOpacity,
  Platform,
  ActivityIndicator,
  ScrollView,
  Alert
} from "react-native";
import { Images } from "../../../../assets/images";
import axios from "axios";
import { contestAPI } from "../../../../config/api";
import { connect } from "react-redux";
import i18n from "../../../../i18n";
import moment from "moment-timezone";
import Modal from "@kalwani/react-native-modal";

class Contest extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      data: "",
      message: "",
      messageModal: false,
      winner: ""
    };
  }

  componentDidMount() {
    let data = this.props.navigation.state.params.contestDetails;
    axios
      .get(contestAPI.contestDetails(data.id, this.props.accessToken), {
        headers: {
          "Accept-Language": i18n.language.includes("es") ? "es" : null
        }
      })
      .then(res => {
        console.log("contestDetails", res.data.data);
        this.setState({ data: res.data.data });
      })
      .catch(res => console.log("ERR", res.response.data));

    if (
      data.contest_status.toUpperCase() == "COMPLETED" ||
      data.contest_status.toUpperCase() == "TERMINADA"
    ) {
      axios
        .get(contestAPI.contestWinner(data.id, this.props.accessToken), {
          headers: {
            "Accept-Language": i18n.language.includes("es") ? "es" : null
          }
        })
        .then(res => {
          console.log("WINNER", res.data);
          this.setState({ winner: res.data.Final_Winner });
        });
    }
  }

  participateInContest = details => {
    console.log("accessToken", this.props.accessToken);
    axios(contestAPI.participateContest(), {
      method: "POST",
      headers: {
        "Accept-Language": i18n.language.includes("es") ? "es" : null
      },
      data: {
        access_token: this.props.accessToken,
        contest_id: details.id
      }
    })
      .then(res => {
        console.log("participate", res.data);
        if (res.data.status_code == 200) {
          details.participated  = true;
          this.setState({ message: res.data.message, messageModal: true });
        }
      }, this.componentDidMount())
      .catch(error => {
        console.log("ERROR", error.response.data);
        Alert.alert(
          error.response.data.message
            ? error.response.data.message
            : "Server Error! Please try again in a while."
        );
      });
  };

  renderButton = details => {

    let contestDetails = details;

    switch (contestDetails.contest_status.toUpperCase()) {
      case "TERMINADA":
      case "COMPLETED":
        return (
          <View>
            <Text style={styles.nameStyle}>{i18n.t("contest:winner")}</Text>
            {this.state.winner ? (
              <View style={styles.userCard}>
                <View
                  style={{
                    flexDirection: "row",
                    justifyContent: "space-between"
                  }}
                >
                  <Image
                    source={{ uri: this.state.winner.image }}
                    style={styles.imageStyle}
                  />
                  <View
                    style={{
                      flexDirection: "column",
                      marginRight: "auto",
                      marginLeft: 10,
                      justifyContent: "center",
                      marginBottom: 10
                    }}
                  >
                    <Text style={styles.nameStyle}>
                      {this.state.winner.full_name}
                    </Text>
                    <Text style={styles.emailStyle}>
                      {i18n.t("contest:winningPoints")} -{" "}
                      {this.state.winner.contest_points}
                    </Text>
                  </View>
                </View>

                <TouchableOpacity
                  style={[styles.button2, { marginTop: 15, marginLeft: "5%" }]}
                  onPress={() =>
                    this.props.navigation.navigate("LeaderBoard", {
                      route: "rankLeaderBoard",
                      contestId: contestDetails.id
                    })
                  }
                >
                  <Text
                    style={{
                      color: "rgb(41,34,108)",
                      textAlign: "center",
                      fontSize: 17,
                      fontFamily: "Comfortaa-Bold",
                      marginBottom: Platform.OS == "android" ? "2%" : 0
                    }}
                  >
                    {i18n.t("contest:rankButton")}
                  </Text>
                </TouchableOpacity>
              </View>
            ) : (
              <View style={styles.userCard}>
                <Text style={styles.nameStyle}>
                  {i18n.t("contest:winnerAnnounced")}
                </Text>
              </View>
            )}
          </View>
        );
      case "PRÓXIMA":
      case "UPCOMING":
        return null;
      case "CORRIENDO":
      case "RUNNING":
        return !contestDetails.participated ? (
          <TouchableOpacity
            style={styles.button}
            onPress={() => this.participateInContest(contestDetails)}
          >
            <Text style={styles.buttonText}>
              {i18n.t("contest:participateButton")}
            </Text>
          </TouchableOpacity>
        ) : (
          <TouchableOpacity
            style={styles.button2}
            onPress={() =>
              this.props.navigation.navigate("LeaderBoard", {
                route: "rankLeaderBoard",
                contestId: contestDetails.id
              })
            }
          >
            <Text
              style={{
                color: "rgb(41,34,108)",
                textAlign: "center",
                fontSize: 17,
                fontFamily: "Comfortaa-Bold",
                marginBottom: Platform.OS == "android" ? "2%" : 0
              }}
            >
              {i18n.t("contest:rankButton")}
            </Text>
          </TouchableOpacity>
        );
      default:
        return console.log("default");
    }
  };

  render() {
    console.log("Props", this.props.navigation.state.params.contestDetails);
    let contestDetails = this.props.navigation.state.params.contestDetails;
    return (
      <View style={{ flex: 1 }}>
        <Modal
          onModalHide={() => null}
          isVisible={this.state.messageModal}
          style={{ flex: 1, justifyContent: "center", alignItems: "center" }}
        >
          <View style={styles.modalStyle}>
            <Image
              source={Images.success}
              resizeMode="contain"
              style={{
                height: 46,
                width: 46,
                alignSelf: "center"
              }}
            />
            <Text
              style={{
                height: "auto",
                width: 250,
                textAlign: "center",
                alignSelf: "center",
                fontFamily: "Comfortaa-Bold",
                color: "rgb(51,51,51)"
              }}
            >
              {this.state.message}
            </Text>
            <TouchableOpacity
              style={{
                height: 44,
                width: 150,
                borderRadius: 8,
                justifyContent: "center",
                alignItems: "center",
                alignSelf: "center",
                backgroundColor: "rgb(41,34,108)",
                marginBottom: 10
              }}
              onPress={() => {
                this.componentDidMount()
                this.setState({ messageModal: false })
              }}
            >
              <Text
                style={{
                  color: "white",
                  textAlign: "center",
                  fontSize: 15,
                  fontFamily: "Comfortaa-Bold",
                  marginBottom: Platform.OS == "android" ? "2%" : 0
                }}
              >
                Ok
              </Text>
            </TouchableOpacity>
          </View>
        </Modal>
        <ImageBackground
          source={Images.loginLayer}
          style={{ flex: 1, alignItems: "center" }}
        >
          <View style={styles.cardStyle}>
            {this.state.data && this.state.data.length > 0 ? (
              <ScrollView
                showsVerticalScrollIndicator={false}
                contentContainerStyle={{
                  paddingHorizontal: 20,
                  paddingVertical: 10,
                  justifyContent: "center",
                  flexGrow: 1
                }}
              >
                <Image source={Images.cup} style={styles.cup} />
                <Text style={styles.title}>
                  {this.state.data[0].contest_name}
                </Text>
                <Text style={styles.description}>
                  {this.state.data[0].contest_details}
                </Text>
                <View
                  style={{ flexDirection: "row", justifyContent: "center" }}
                >
                  <View
                    style={{
                      marginLeft: "auto",
                      marginRight: "auto",
                      marginHorizontal: 10,
                      marginVertical: 20
                    }}
                  >
                    <View style={{ marginVertical: 10 }}>
                      <Image source={Images.calendar} style={styles.icon} />
                      <Text style={styles.iconTitle}>
                        {i18n.t("contest:startDate")}
                      </Text>
                      <Text style={styles.iconDescription}>
                        {moment
                          .tz(
                            this.state.data[0].contest_startdate,

                            "America/New_York"
                          )
                          .format("YYYY-MM-DD hh:mm A")}
                      </Text>
                    </View>

                    <View style={{ marginVertical: 10 }}>
                      <Image
                        source={Images.dollar}
                        style={styles.icon}
                        resizeMode="contain"
                      />
                      <Text style={styles.iconTitle}>
                        {this.state.data[0].contest_point}
                      </Text>
                      <Text style={styles.iconDescription}>
                        {i18n.t("contest:rewardPoints")}
                      </Text>
                    </View>
                  </View>

                  <View
                    style={{
                      marginLeft: "auto",
                      marginRight: "auto",
                      marginHorizontal: 10,
                      marginVertical: 20
                    }}
                  >
                    <View style={{ marginVertical: 10 }}>
                      <Image source={Images.calendar} style={styles.icon} />
                      <Text style={styles.iconTitle}>
                        {i18n.t("contest:endDate")}
                      </Text>
                      <Text style={styles.iconDescription}>
                        {moment
                          .tz(
                            this.state.data[0].contest_enddate,
                            "America/New_York"
                          )
                          .format("YYYY-MM-DD hh:mm A")}
                      </Text>
                    </View>

                    <View style={{ marginVertical: 10 }}>
                      <Image
                        source={Images.participate}
                        style={styles.icon}
                        resizeMode="contain"
                      />
                      <Text style={styles.iconTitle}>
                        {this.state.data[0].total_user}
                      </Text>
                      <Text style={styles.iconDescription}>
                        {i18n.t("contest:participants")}
                      </Text>
                    </View>
                  </View>
                </View>
                {this.renderButton(contestDetails)}
              </ScrollView>
            ) : (
              <View style={{ justifyContent: "center", alignItems: "center" }}>
                <ActivityIndicator size="large" />
              </View>
            )}
          </View>
        </ImageBackground>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  cardStyle: {
    height: "95%",
    width: "90%",
    elevation: 5,
    backgroundColor: "white",
    borderRadius: 10,
    shadowColor: "#000",
    shadowOffset: { width: 0, height: 1 },
    shadowOpacity: 0.5,
    shadowRadius: 2,
    justifyContent: "center",
    overflow: Platform.OS == "ios" ? "visible" : "hidden"
  },
  cup: {
    height: 126,
    width: 116,
    alignSelf: "center",
    marginTop: 10
  },
  title: {
    fontSize: 20,
    color: "rgb(51,51,51)",
    alignSelf: "center",
    marginVertical: 15
  },
  description: {
    width: 272,
    textAlign: "center",
    alignSelf: "center",
    color: "rgb(51,51,51)"
  },
  icon: {
    height: 34,
    width: 36,
    alignSelf: "center",
    marginVertical: 10
  },
  iconTitle: {
    color: "rgb(102,102,102)",
    fontSize: 12,
    textAlign: "center",
    alignSelf: "center"
  },
  iconDescription: {
    color: "rgb(51,51,51)",
    fontSize: 13,
    textAlign: "center",
    alignSelf: "center"
  },
  button: {
    height: 45,
    width: "100%",
    borderRadius: 8,
    justifyContent: "center",
    backgroundColor: "rgb(41,34,108)"
  },
  button2: {
    height: 45,
    width: "100%",
    borderRadius: 8,
    borderWidth: 1,
    borderColor: "rgb(41,34,108)",
    justifyContent: "center",
    backgroundColor: "white"
  },
  buttonText: {
    color: "white",
    textAlign: "center",
    fontSize: 17,
    fontFamily: "Comfortaa-Bold",
    marginBottom: Platform.OS == "android" ? "2%" : 0
  },
  modalStyle: {
    height: 180,
    justifyContent: "space-evenly",
    width: 295,
    backgroundColor: "white",
    borderRadius: 10
  },
  userCard: {
    flex: 1,
    width: "92%",
    marginVertical: 15
  },
  nameStyle: {
    fontSize: 16,
    color: "rgb(51,51,51)",
    fontFamily: "Comfortaa-Bold",
    marginVertical: Platform.OS == "ios" ? 10 : 0
  },
  imageStyle: {
    height: 70,
    width: 70,
    marginRight: 5,
    borderRadius: Platform.OS == "ios" ? 35 : 100
  },
  emailStyle: {
    fontFamily: "Comfortaa-Bold",
    fontSize: 13,
    textAlign: "justify"
  }
});

const mapStateToProps = ({ auth }) => {
  const { accessToken } = auth;
  return { accessToken };
};

export default connect(mapStateToProps, null)(Contest);
