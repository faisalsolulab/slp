import React from "react";
import {
  Text,
  View,
  ScrollView,
  ImageBackground,
  StyleSheet,
  FlatList,
  Image,
  TouchableOpacity,
  ActivityIndicator,
  Platform
} from "react-native";
import { connect } from "react-redux";
import { getUserRewards, fetchDashBoard } from "../../../Actions";
import moment from "moment";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp
} from "react-native-responsive-screen";
import { NavigationEvents } from "react-navigation";
import { Images } from "../../../assets/images";
import i18n from "i18next";
import NoConnectionModalReward from "../../../utility/NoConnectionModal";

class RewardPoints extends React.Component {
  static navigationOptions = ({ navigation }) => {
    return {
      headerTitle: (
        <Text
          style={{
            color: "white",
            fontSize: 20,
            alignSelf: "center",
            marginLeft: "auto",
            marginRight: "auto",
            fontFamily: "Comfortaa-Bold"
          }}
        >
          {i18n.t("title:rewardPoints")}
        </Text>
      ),
      headerStyle: {
        backgroundColor: "rgb(41,34,108)",
        borderBottomWidth: 0,
        height: hp("7%"),
        elevation: 0
      }
    };
  };

  constructor(props) {
    super(props);
    this.state = {
      refreshing: false,
      langReward: ""
    };
  }

  async componentDidMount() {
    this.setState({ langReward: i18n.language });
    // await this.props.getUserRewards(this.props.accessToken);
  }

  getUserdata = async () => {
    await this.props.getUserRewards(this.props.accessToken);
  };

  refreshData = async () => {
    await this.props.getUserRewards(this.props.accessToken);
    setTimeout(async () => {
      if (!this.props.rewardsFail) {
        await this.props.fetchDashBoard(this.props.accessToken);
      }
    }, 4000);
  };

  render() {
    console.log("i17", i18n.language);
    return (
      <View style={{ flex: 1 }}>
        <ImageBackground
          source={require("../../../assets/loginLayer.jpg")}
          style={{ flex: 1 }}
        >
          <View style={{ flex: 1, alignItems: "center" }}>
            <NavigationEvents
              onWillFocus={() => {
                if (i18n.language !== this.state.langReward) {
                  this.setState({ langReward: i18n.language });
                  this.getUserdata();
                }
                //Call whatever logic or dispatch redux actions and update the screen!
              }}
            />
            <NoConnectionModalReward
              onPress={this.getUserdata}
              errorMessage={this.props.rewardsFailMessage}
              isVisible={this.props.rewardsFail}
            />

            <View style={styles.cardStyle}>
              <View
                style={[
                  styles.headerStyle,
                  { height: i18n.language.includes("es") ? 95 : 85 }
                ]}
              >
                <View
                  style={{
                    flexDirection: "row",
                    justifyContent: "space-between",
                    alignItems: "center"
                  }}
                >
                  <Text
                    style={{
                      fontSize: i18n.language.includes("es") ? 15 : 16,
                      fontFamily: "Comfortaa-Bold",
                      flexShrink: 1,
                      flexWrap: "wrap"
                    }}
                  >
                    {i18n.t("common:lifetimeEarnedPoints")}
                  </Text>
                  {this.props.dashboardData ? (
                    <Text
                      style={{
                        fontFamily: "Comfortaa-Bold",
                        fontSize: i18n.language.includes("es") ? 15 : 16,
                        color: "black"
                      }}
                    >
                      {this.props.dashboardData.lifetime_earned_points}
                    </Text>
                  ) : (
                    <ActivityIndicator
                      size={"small"}
                      color={"rgb(41,34,108)"}
                    />
                  )}
                </View>
                <View
                  style={{
                    borderWidth: 0.6,
                    opacity: 0.1,
                    margin: "2%",
                    marginBottom: "2%"
                  }}
                />
                <View
                  style={{
                    flexDirection: "row",
                    justifyContent: "space-between"
                  }}
                >
                  <Text
                    style={{
                      fontSize: i18n.language.includes("es") ? 15 : 16,
                      fontFamily: "Comfortaa-Bold",
                      flexShrink: 1
                    }}
                  >
                    {i18n.t("common:availablePointsToRedeem")}
                  </Text>
                  {this.props.dashboardData ? (
                    <Text
                      style={{
                        fontFamily: "Comfortaa-Bold",
                        fontSize: i18n.language.includes("es") ? 15 : 16,
                        color: "black"
                      }}
                    >
                      {this.props.dashboardData.available_points}
                    </Text>
                  ) : (
                    <ActivityIndicator
                      size={"small"}
                      color={"rgb(41,34,108)"}
                    />
                  )}
                </View>
              </View>
              <View style={{ margin: "2%" }} />
              <View style={{ flex: 9 }}>
                {this.props.rewardsData.length > 0 ? (
                  <FlatList
                    data={this.props.rewardsData}
                    extraData={this.props.rewardsData}
                    onRefresh={() => this.refreshData()}
                    refreshing={this.state.refreshing}
                    renderItem={({ item }) => {
                      return (
                        <View
                          style={{
                            width: "98%",
                            marginLeft: 10,
                            marginRight: 0,
                            marginTop: 0,
                            height: 100,
                            justifyContent: "center",
                            marginBottom: 5,
                            borderBottomWidth: 0.3,
                            borderRadius: 10
                          }}
                        >
                          <View
                            style={{
                              flexDirection: "row",
                              alignItems: "center"
                            }}
                          >
                            <Image
                              source={{ uri: item.product_default_img }}
                              style={styles.imageStyle}
                              resizeMode="contain"
                            />
                            <View style={{ flexDirection: "column" }}>
                              <Text
                                style={{
                                  fontSize: 17,
                                  fontFamily: "Comfortaa-Bold",
                                  color: "black"
                                }}
                              >
                                {item.reward_type}
                              </Text>
                              <Text
                                style={{
                                  fontSize: 13,
                                  fontFamily: "Comfortaa-Bold",
                                  color: "black",
                                  marginVertical: 5
                                }}
                              >
                                {item.reward_name}
                              </Text>
                              <View style={{ flexDirection: "row" }}>
                                <Text
                                  style={{
                                    fontFamily: "Comfortaa-Bold",
                                    marginRight: 5
                                  }}
                                >
                                  {moment
                                    .utc(item.updated_at)
                                    .local()
                                    .format("YYYY-MM-DD hh:mm A")}
                                </Text>
                                {/* <Text style={{ fontFamily: "Comfortaa-Bold" }}>
                                {item.time}
                              </Text> */}
                              </View>
                            </View>
                            <Text
                              style={{
                                fontFamily: "Comfortaa-Bold",
                                marginRight: 12,
                                marginTop: 3,
                                marginLeft: "auto",
                                paddingLeft: 10,
                                color: item.points_type.includes("Credit")
                                  ? "green"
                                  : "red"
                              }}
                            >
                              {item.reward_points}
                            </Text>
                          </View>
                        </View>
                      );
                    }}
                    keyExtractor={item => item.id.toString()}
                    showsVerticalScrollIndicator={false}
                    style={{ marginBottom: "4%" }}
                  />
                ) : !this.props.rewardsData ? (
                  <ActivityIndicator
                    size={"large"}
                    color={"rgb(41,34,108)"}
                    style={{ marginTop: "auto", marginBottom: "auto" }}
                  />
                ) : (
                  <View
                  style={{
                    flex: 1,
                    justifyContent: "center",
                    alignItems: "center"
                  }}
                >
  <Text> {i18n.t("rewardsPoint:noEarned")}</Text>
                </View>
  
                )}
              </View>
              <TouchableOpacity
                style={styles.button}
                onPress={() => this.props.navigation.navigate("Gift")}
              >
                <Text
                  style={{
                    color: "white",
                    textAlign: "center",
                    fontSize: 16,
                    fontFamily: "Comfortaa-Bold",
                    marginBottom: Platform.OS == "ios" ? 0 : 5
                  }}
                >
                  {i18n.t("common:cashOut")}
                </Text>
              </TouchableOpacity>
              <View style={{ margin: "1%" }} />
            </View>
          </View>
        </ImageBackground>
      </View>
    );
  }
}

const styles = StyleSheet.create({
  cardStyle: {
    height: "97%",
    width: "90%",
    padding: "2%",
    marginBottom: "4%",
    paddingTop: 10,
    paddingBottom: 0,
    elevation: 6,
    backgroundColor: "white",
    borderRadius: 10,
    shadowColor: "#000",
    shadowOffset: { width: 0, height: 1 },
    shadowOpacity: 0.5,
    shadowRadius: 2
  },
  headerStyle: {
    width: "95%",
    padding: "3%",
    paddingTop: 2,
    paddingBottom: 2,
    marginTop: "1.5%",
    backgroundColor: "rgb(229,227,237)",
    borderRadius: 8,
    alignSelf: "center",
    justifyContent: "center"
  },
  titleStyle: {
    fontSize: 17,
    fontFamily: "Comfortaa-Bold",
    color: "black",
    flexShrink: 1,
    flexWrap: "wrap"
  },
  imageStyle: {
    height: "150%",
    width: "25%",
    marginRight: "4%",
    borderRadius: 10
  },
  button: {
    height: 47,
    width: "100%",
    borderRadius: 8,
    justifyContent: "center",
    backgroundColor: "rgb(41,34,108)"
  }
});

const mapStateToProps = ({ auth, barrel, home }) => {
  const {
    rewardsData,
    rewardsFail,
    rewardsFailMessage,
    rewardLoading
  } = barrel;
  const { dashboardData } = home;
  const { userData, accessToken, userId } = auth;
  return {
    accessToken,
    rewardLoading,
    userId,
    rewardsData,
    rewardsFail,
    rewardsFailMessage,
    userData,
    dashboardData
  };
};

const actionCreater = {
  getUserRewards,
  fetchDashBoard
};

export default connect(mapStateToProps, actionCreater)(RewardPoints);
