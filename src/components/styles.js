import React from 'react';
import {StyleSheet,Platform} from 'react-native';
import { heightPercentageToDP as hp, widthPercentageToDP as wp } from 'react-native-responsive-screen';

export const MainStyles = StyleSheet.create({
    cardStyle: {
        height:'95%',
        width: "90%",
        padding:'2%',
        paddingBottom: 0,
        paddingTop: 10,
        elevation: 5,
        backgroundColor: "white",
        borderRadius: 10,
        shadowColor: "#000",
        shadowOffset: { width: 0, height: 1 },
        shadowOpacity: 0.5,
        shadowRadius: 2,
        alignSelf:'center'
      },
      container:{
        flex:1, 
        alignItems:'center'
      },
      titleStyle:{
          fontSize:15,
          fontFamily:'Comfortaa-Bold',
          color:'rgb(51,51,51)',
          marginLeft:hp('0.5%'),
          marginVertical:5
      },
      subtitleStyle:{
          color:'rgba(102,102,102, 0.6)',
          fontFamily:'Comfortaa-Bold',
          textAlign:'justify',
          fontSize:15
      },
      borderStyle:{
        borderWidth:0.6,   
        borderColor: "rgba(200,200,200,0.3)",
      },
      productImage:{
        height:hp('11%'),
        width:hp('11%'),
    },
    addUserIconStyle:{
        height:hp('3%')
    },
    userCard:{
        height:200,
        width:wp('80%'),
        alignSelf:'center',
        elevation: 5,
        backgroundColor: "white",
        borderRadius: 10,
        shadowColor: "#000",
        shadowOffset: { width: 0, height: 1 },
        shadowOpacity: 0.5,
        shadowRadius: 2,
        marginBottom:hp('1%'),
        marginTop:hp('0.5%'),
        borderWidth:0.6,
        borderColor:'rgba(41,34,108,0.1)',
        justifyContent:'center'
    },
    textInputStyle: {
        flex:1,
        width:wp('72%'),
        paddingLeft:hp('1%'),
        flexDirection:'row',
        fontFamily:'Comfortaa-Bold',
        borderWidth:0.6,
        fontWeight:'normal',
        marginLeft:'auto',
        marginRight:'auto',
        alignItems:'center',
        // justifyContent:'center',
        borderColor: "rgba(200,200,200,1)",
        borderRadius: 5
      },
      button: {
        height: 47,
        width: wp('75%'),
        alignSelf:'center',
        borderRadius: 8,
        justifyContent: "center",
        backgroundColor: "rgb(41,34,108)"
      },
      buttonText:{
          fontFamily:'Comfortaa-Bold',
          fontSize:17,
          color:'white',
          textAlign:'center',
      }
})