import React, { Component } from "react";
import {
  View,
  Text,
  ImageBackground,
  TouchableOpacity,
  ScrollView,
  Image,
  StyleSheet,
  Platform
} from "react-native";
import { connect } from "react-redux";
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view";
import Autocomplete from "react-native-autocomplete-input";
import axios from "axios";
import { TextInput, Checkbox } from "react-native-paper";
import {
  heightPercentageToDP as hp,
  widthPercentageToDP as wp
} from "react-native-responsive-screen";
import { Icon } from "react-native-elements";
import {
  registerUser,
  nameChanged,
  lastNameChanged,
  resetData,
  companyNameChanged,
  passwordChanged,
  emailChanged,
  numberChanged,
  getCompanyNames,
  checkTerms
} from "../../Actions";
import i18n from "../../i18n";
import { SafeAreaView } from "react-navigation";
class Register extends Component {
  constructor(props) {
    super(props);
    this.state = {
      emailErr: false,
      lastnameErr: false,
      nameErr: false,
      numErr: false,
      compErr: false,
      passErr: false,
      data: [],
      companyName: ""
    };
  }

  componentWillMount() {
    this.props.resetData();
  }

  async componentDidMount() {
    await this.props.getCompanyNames();
  }

  onEmailChange(text) {
    this.props.emailChanged(text);
  }
  onLastNameChange(text) {
    this.props.lastNameChanged(text);
  }

  onCompanyNameChanged(text) {
    this.props.companyNameChanged(text);
  }

  onNameChange(text) {
    this.props.nameChanged(text);
  }

  onNumberChange(text) {
    this.props.numberChanged(text);
  }

  onPasswordChange(text) {
    this.props.passwordChanged(text);
  }

  _filterData = queryData => {
    const newData = this.props.companyList.filter(item => {
      const itemData = `${item.toUpperCase()}`;
      const textData = queryData.toUpperCase();

      // return itemData.indexOf(textData) > -1;
      return itemData.includes(textData);
    });
    if (queryData !== "") {
      this.setState({
        data: newData.slice(0, 3)
      });
      this.props.companyNameChanged(queryData);
    } else {
      this.setState({ data: [] });
      this.props.companyNameChanged("");
    }
  };

  ValidateEmail = mail => {
    if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(mail)) {
      return true;
    }
    return false;
  };

  clearList = () => {
    this.setState({ data: [] });
  };

  submit = async () => {
    const { name, lastname, email, number, companyName, password } = this.props;
    if (name.length > 1) {
      this.setState({ nameErr: false });
      if (lastname.length > 1) {
        this.setState({ lastnameErr: false });
        if (this.ValidateEmail(email)) {
          this.setState({ emailErr: false });
          if (number.length > 9 && number.length < 11) {
            this.setState({ numErr: false });
            if (companyName.length > 1) {
              this.setState({ compErr: false });
              if (password.length > 5) {
                this.setState({ passErr: false });
                this.props.navigation.navigate("RegisterSecond");
              } else {
                this.setState({ passErr: true });
              }
            } else {
              this.setState({ compErr: true });
            }
          } else {
            this.setState({ numErr: true });
          }
        } else {
          this.setState({ emailErr: true });
        }
      } else {
        this.setState({ lastnameErr: true });
      }
    } else {
      this.setState({ nameErr: true });
    }
  };

  render() {
    const { data } = this.state;
    //  const companyNameData = this._filterData(companyName);
    // let comp = (a, b) => a.toLowerCase().trim() === b.toLowerCase().trim();
    return (
      <SafeAreaView
        style={{ flex: 1, backgroundColor: "rgb(41,34,108)" }}
        forceInset={{ bottom: "never" }}
      >
        <KeyboardAwareScrollView
          enableOnAndroid
          extraHeight={200}
          keyboardShouldPersistTaps={"handled"}
          style={{backgroundColor:'white'}}
        >
            <ImageBackground
              style={{ height: hp("23.5%"), flex: 1 }}
              source={require("../../assets/registerbg.png")}
              resizeMode="stretch"
            >
              <TouchableOpacity
                style={{
                  flex: 1,
                  height: 200,
                  marginTop: Platform.OS == "ios" ? 25 : 20
                }}
                activeOpacity={0.8}
              >
                <Icon
                  name="left"
                  type="antdesign"
                  iconStyle={{ color: "white" }}
                  underlayColor="rgba(255, 255, 255, 0)"
                  onPress={() => this.props.navigation.goBack()}
                  containerStyle={{
                    flex: 1,
                    width: 60,
                    alignSelf: "flex-start"
                  }}
                />
              </TouchableOpacity>
              <View
                style={{
                  flex: 5,
                  justifyContent: "flex-start",
                  transform: [{ translateY: -10 }]
                }}
              >
                <Image
                  source={require("../../assets/logo.png")}
                  style={styles.logoStyle}
                  resizeMode="contain"
                />
              </View>
            </ImageBackground>

            <View style={{ flex: 1 }}>
              <Text style={styles.titleStyle}>
                {i18n.t("register:register")}
              </Text>
              <View
                style={{
                  flex: 1,
                  marginLeft: "5%",
                  marginRight: "5%",
                  justifyContent: "center"
                }}
              >
                <TextInput
                  autoCapitalize="words"
                  placeholder={i18n.t("register:enterFirstName")}
                  label={
                    this.state.nameErr
                      ? i18n.t("register:validFirstName")
                      : i18n.t("register:firstName")
                  }
                  value={this.props.name}
                  style={styles.textInputStyle}
                  onChangeText={this.onNameChange.bind(this)}
                  theme={{ colors: { primary: "#29226c" } }}
                  error={this.state.nameErr}
                />
                <TextInput
                  autoCapitalize="words"
                  placeholder={i18n.t("register:enterLastName")}
                  label={
                    this.state.lastnameErr
                      ? i18n.t("register:validLastName")
                      : i18n.t("register:lastName")
                  }
                  value={this.props.lastname}
                  style={styles.textInputStyle}
                  onChangeText={this.onLastNameChange.bind(this)}
                  theme={{ colors: { primary: "#29226c" } }}
                  error={this.state.lastnameErr}
                />
                <TextInput
                  placeholder={i18n.t("login:enterEmail")}
                  autoCapitalize="none"
                  label={
                    this.state.emailErr
                      ? i18n.t("login:enterValidEmail")
                      : i18n.t("login:emailAddress")
                  }
                  keyboardType="email-address"
                  value={this.props.email}
                  onChangeText={this.onEmailChange.bind(this)}
                  style={styles.textInputStyle}
                  theme={{ colors: { primary: "#29226c" } }}
                  error={this.state.emailErr}
                />
                <TextInput
                  placeholder={i18n.t("register:enterTelephoneNumber")}
                  label={
                    this.state.numErr
                      ? i18n.t("register:validNumber")
                      : i18n.t("register:telephone")
                  }
                  value={this.props.number}
                  keyboardType="number-pad"
                  onChangeText={this.onNumberChange.bind(this)}
                  style={[styles.textInputStyle, { marginBottom: "2%" }]}
                  theme={{ colors: { primary: "#29226c" } }}
                  error={this.state.numErr}
                />
                <View style={styles.autocompleteContainer}>
                  <Autocomplete
                    autoCapitalize="sentences"
                    keyExtractor={(item, index) => index.toString()}
                    autoCorrect={false}
                    inputContainerStyle={{ borderColor: "white" }}
                    // containerStyle={styles.autocompleteContainer}
                    listStyle={{
                      borderWidth: 1,
                      borderColor: "rgb(41,34,108)",
                      padding: 5,
                      borderBottomLeftRadius: 5,
                      borderBottomRightRadius: 5
                    }}
                    data={data}
                    renderItem={({ item, index }) => {
                      console.log(item);
                      return (
                        <TouchableOpacity
                          onPress={() => {
                            this.setState({ data: [] });
                            this.props.companyNameChanged(item);
                          }}
                          activeOpacity={0.6}
                        >
                          <Text style={styles.itemText}>{item}</Text>
                        </TouchableOpacity>
                      );
                    }}
                    renderTextInput={() => (
                      <TextInput
                        {...this.props}
                        placeholder={i18n.t("register:enterCompanyName")}
                        label={
                          this.state.compErr
                            ? i18n.t("register:validCompanyName")
                            : i18n.t("register:companyName")
                        }
                        value={this.props.companyName}
                        onEndEditing={() => this.clearList()}
                        onChangeText={text => this._filterData(text)}
                        style={styles.textInputStyle}
                        theme={{ colors: { primary: "#29226c" } }}
                        error={this.state.compErr}
                      />
                    )}
                  />
                </View>
                <TextInput
                  placeholder={i18n.t("login:enterPassword")}
                  label={
                    this.state.passErr
                      ? i18n.t("login:passwordContain")
                      : i18n.t("login:password")
                  }
                  value={this.props.password}
                  secureTextEntry
                  onChangeText={this.onPasswordChange.bind(this)}
                  style={[
                    styles.textInputStyle,
                    { marginTop: "2%", marginBottom: "5%" }
                  ]}
                  theme={{ colors: { primary: "#29226c" } }}
                  error={this.state.passErr}
                />
              </View>
              <TouchableOpacity
                onPress={this.submit}
                style={styles.button}
                activeOpacity={0.6}
              >
                <Text style={styles.loginText}>
                  {i18n.t("register:continue")}
                </Text>
              </TouchableOpacity>
              <View
                style={{
                  flexDirection: "row",
                  justifyContent: "center",
                  marginTop: 5
                  // marginBottom: 0,
                }}
              >
                <Text
                  style={{
                    fontFamily: "Comfortaa-Bold",
                    color: "black",
                    fontSize: 15
                  }}
                >
                  {i18n.t("register:alreadyRegister")}{" "}
                </Text>
                <TouchableOpacity
                  onPress={() => this.props.navigation.navigate("Login")}
                  activeOpacity={0.6}
                >
                  <Text
                    style={{
                      color: "rgb(41,34,108)",
                      textDecorationLine: "underline",
                      fontFamily: "Comfortaa-Bold",
                      fontSize: 15
                    }}
                  >
                    {i18n.t("login:login")}
                  </Text>
                </TouchableOpacity>
              </View>
            </View>
        </KeyboardAwareScrollView>
      </SafeAreaView>
    );
  }
}

const styles = StyleSheet.create({
  itemText: {
    fontSize: 20,
    color: "black"
  },
  autocompleteContainer: {
    flex: 1,
    left: 0,
    position: "relative",
    right: 0,
    // top: '58.5%',
    zIndex: 1
  },
  logoStyle: {
    height: "70%",
    width: "100%"
  },
  subTitle: {
    color: "white",
    margin: "2%",
    marginTop: 0,
    alignSelf: "center",
    fontSize: 15,
    fontFamily: "Comfortaa-Bold"
  },
  textInputStyle: {
    textAlign: "center",
    height: hp("7.8%"),
    fontFamily: "Comfortaa-Bold",
    justifyContent: "center",
    backgroundColor: "white"
  },
  titleStyle: {
    bottom: "3%",
    alignSelf: "center",
    fontSize: 27,
    color: "black",
    fontFamily: "Comfortaa-Bold"
  },
  button: {
    height: 47,
    width: "90%",
    alignSelf: "center",
    borderRadius: 8,
    justifyContent: "center",
    backgroundColor: "rgb(41,34,108)"
  },
  loginText: {
    color: "white",
    fontFamily: "Comfortaa-Bold",
    fontSize: 20,
    alignSelf: "center",
    paddingBottom: Platform.OS == "ios" ? 0 : 8
  }
});

const mapStateToProps = ({ auth }) => {
  const {
    loading,
    companyList,
    error,
    added,
    success,
    name,
    lastname,
    number,
    companyName,
    termsChecked,
    email,
    password
  } = auth;
  return {
    loading,
    companyList,
    error,
    added,
    success,
    name,
    lastname,
    number,
    companyName,
    termsChecked,
    email,
    password
  };
};

const actionCreators = {
  registerUser,
  nameChanged,
  lastNameChanged,
  companyNameChanged,
  passwordChanged,
  emailChanged,
  numberChanged,
  checkTerms,
  getCompanyNames,
  resetData
};

export default connect(mapStateToProps, actionCreators)(Register);
