import React, { Component } from "react";
import {
  Alert,
  ActivityIndicator,
  View,
  Text,
  ImageBackground,
  TouchableOpacity,
  Image,
  StyleSheet,
  Platform
} from "react-native";
import { TextInput } from "react-native-paper";
import Modal from "react-native-modal";
import NetInfo from "@react-native-community/netinfo";
import NoConnection from "../../utility/NoConnectionAuth";
import { heightPercentageToDP as hp } from "react-native-responsive-screen";
import { Icon } from "react-native-elements";
import axios from "axios";
import { BASE_URL, authApi } from "../../config/api";
import i18n from "../../i18n";
import {SafeAreaView} from 'react-navigation'

class ForgotPassword extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      email: "",
      error: false,
      isModalVisible: false,
      correct: false,
      errorMessage: "",
      connected: true
    };
  }

  componentDidMount() {
    this.unsubscribe = NetInfo.addEventListener(state => {
      console.log("Connection type", state.type);
      console.log("Is connected?", state.isConnected);
      if (!state.isConnected) {
        setTimeout(() => {
          this.setState({ connected: false });
        }, 500);
      } else {
        setTimeout(() => {
          this.setState({ connected: true });
        }, 500);
      }
    });
  }

  componentWillUnmount() {
    this.unsubscribe();
  }

  emailChanged = text => {
    this.setState({ email: text });
  };

  loading = () => {
    this.setState({ loading: true, error: false });
    this.forgetSubmit();
  };

  ValidateEmail = mail => {
    if (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(mail)) {
      return true;
    }
    return false;
  };

  forgetSubmit = async () => {
    if (this.ValidateEmail(this.state.email)) {
      const data = {
        email: this.state.email
      };
      await axios
        .post(BASE_URL + authApi.forgotPass, data, {
          headers: {
            "Content-Type": "application/json",
            'Accept-Language': i18n.language.includes('es') ? 'es' : null
          }
        })
        .then(res => {
          console.log("forgot pass res", res);
          this.setState({
            isModalVisible: !this.state.isModalVisible,
            loading: false,
            error: false,
            correct: true
          });
        })
        .catch(err => {
          console.log("Error", err.response.data);
          this.setState({
            isModalVisible: !this.state.isModalVisible,
            errorMessage: err.response.data.message,
            loading: false,
            error: false,
            correct: false
          });
        });
    } else {
      this.setState({ loading: false, error: true });
    }
  };

  toggleModal = () => {
    this.setState({ isModalVisible: !this.state.isModalVisible });
  };

  modalRender() {
    return (
      <View>
        <Modal isVisible={this.state.isModalVisible} style={styles.modalStyle}>
          <View style={styles.cardStyle}>
            {this.state.correct ? (
              <Image
                source={require("../../assets/Icons/success.png")}
                resizeMode="contain"
                style={{
                  height: "20%",
                  alignSelf: "center"
                }}
              />
            ) : (
              <Image
                source={require("../../assets/Icons/Alert.png")}
                resizeMode="contain"
                style={{
                  height: "20%",
                  alignSelf: "center"
                }}
              />
            )}
            <Text
              style={{
                textAlign: "center",
                fontFamily: "Comfortaa-Bold",
                color: "rgb(51,51,51)",
                margin: "5%"
              }}
            >
              {this.state.correct
                ? i18n.t("forgetPassword:title")
                : this.state.errorMessage}
            </Text>
            <TouchableOpacity
              style={[styles.buttonModal, { height: "30%" }]}
              onPress={() => this.toggleModal()}
              activeOpacity={0.6}
            >
              <Text
                style={{
                  color: "white",
                  textAlign: "center",
                  fontSize: 15,
                  fontFamily: "Comfortaa-Bold"
                }}
              >
                OK
              </Text>
            </TouchableOpacity>
          </View>
        </Modal>
      </View>
    );
  }

  renderButton() {
    if (this.state.loading) {
      return (
        <View style={styles.button}>
          <ActivityIndicator size="large" />
        </View>
      );
    }
    return (
      <View style={styles.button}>
        <TouchableOpacity onPress={this.loading} activeOpacity={0.6}>
          <Text style={styles.loginText}>{i18n.t("common:submit")}</Text>
        </TouchableOpacity>
      </View>
    );
  }

  render() {
    if (this.state.connected) {
      return (
        <SafeAreaView style={{flex:1, backgroundColor:'rgb(41,34,108)'}} forceInset={{bottom:'never'}}>
          <ImageBackground
            source={require("../../assets/forgetLayer.jpg")}
            style={{ flex: 1}}
          >
            <Icon
              name="left"
              type="antdesign"
              iconStyle={{ color: "white" }}
              onPress={() => this.props.navigation.goBack()}
              underlayColor="rgba(255, 255, 255, 0)"
              containerStyle={{
                flex: 1,
                width: 60,
                paddingTop: 15,
                alignSelf: "flex-start"
              }}
            />
            <View style={{ flex: 4, justifyContent: "center" }}>
              <Image
                source={require("../../assets/logo.png")}
                resizeMode="contain"
                style={styles.logo}
              />
            </View>
            <View style={{ flex: 6, backgroundColor:'white' }}>
              {this.modalRender()}
              <View style={{ flex: 2, justifyContent: "center" }}>
                <Text style={styles.titleStyle}>{i18n.t("forgetPassword:forgotPassword")}</Text>
                <Text style={{ margin: "6%", marginTop: 10, marginBottom: 5 }}>
                  {i18n.t("forgetPassword:title")}
                </Text>
              </View>
              <View style={{ flex: 5, alignItems: "center" }}>
                <TextInput
                  placeholder={i18n.t("login:enterEmail")}
                  label={this.state.error ? i18n.t("forgetPassword:invalid") : i18n.t("login:emailAddress")}
                  autoCapitalize="none"
                  value={this.state.email}
                  keyboardType="email-address"
                  onChangeText={text => this.emailChanged(text)}
                  style={styles.textInputStyle}
                  error={this.state.error}
                  theme={{ colors: { primary: "#29226c" } }}
                />
                {this.renderButton()}
              </View>
            </View>
          </ImageBackground>
        </SafeAreaView>
      );
    }
    return (
      <NoConnection
        onPress={() => {
          this.componentDidMount();
        }}
      />
    );
  }
}

const styles = StyleSheet.create({
  logo: {
    height: "60%",
    width: "100%",
    marginBottom: "15%"
  },
  subTitle: {
    color: "white",
    margin: "2%",
    marginTop: 0,
    fontFamily: "Comfortaa-Bold",
    alignSelf: "center",
    fontSize: 22
  },
  textInputStyle: {
    fontFamily: "Comfortaa-Bold",
    backgroundColor: "white",
    height: hp("9%"),
    width: "90%"
  },
  titleStyle: {
    alignSelf: "center",
    fontSize: 27,
    color: "black",
    fontFamily: "Comfortaa-Bold"
  },
  button: {
    height: 47,
    width: "90%",
    borderRadius: 8,
    marginTop: "5%",
    justifyContent: "center",
    alignSelf: "center",
    backgroundColor: "rgb(41,34,108)"
  },
  loginText: {
    color: "white",
    fontFamily: "Comfortaa-Bold",
    fontSize: 20,
    alignSelf: "center",
    paddingBottom: Platform.OS == "ios" ? 0 : 8
  },
  modalStyle: {
    justifyContent: "center",
    alignItems: "center"
  },
  buttonModal: {
    height: "10%",
    width: "100%",
    borderRadius: 8,
    justifyContent: "center",
    backgroundColor: "rgb(41,34,108)"
  },
  cardStyle: {
    height: hp("30%"),
    justifyContent: "center",
    width: "90%",
    padding: "5%",
    paddingBottom: 0,
    elevation: 6,
    backgroundColor: "white",
    borderRadius: 10
  }
});

export default ForgotPassword;
