import React from "react";
import { Text } from "react-native";
import {
  createAppContainer,
  createStackNavigator,
} from "react-navigation";
import ScanCode from "./components/MainScreens/TopNavigatorScreens/ScanCode";
import InsulationSpray from "./components/MainScreens/TopNavigatorScreens/Insulation";
import LearnToEarn from "./components/MainScreens/TopNavigatorScreens/LearnToEarn";
import Support from "./components/MainScreens/TopNavigatorScreens/Support";
import Home from "./components/MainScreens/Home";

const styles = {
  labelStyle: {
    fontSize: 12,
    fontFamily: "Comfortaa-Bold",
    textAlign: "center"
  }
};

const topNavigator = createStackNavigator(
  {
    Home:{
      screen:Home
    },
    ScanCode: {
      screen: ScanCode,
    },
    InsulationSpray: {
      screen: InsulationSpray,
    },
    LearnToEarn: {
      screen: LearnToEarn,
    },
    Support: {
      screen: Support,
    }
  },
  {
    headerMode:'none'
  }
);

const HomeNavigator = createAppContainer(topNavigator);

export default HomeNavigator;
