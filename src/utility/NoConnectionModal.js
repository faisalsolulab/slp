import React from "react";
import { StyleSheet, Text, TouchableOpacity, View, Image } from "react-native";
import Modal from "react-native-modal";
import {
  widthPercentageToDP as wp,
  heightPercentageToDP as hp,
} from "react-native-responsive-screen";
import i18n from "../i18n";

export default NoConnectionModal = ({
  onPress,
  errorMessage,
  isVisible,
  modalStyle = styles.modalStyle,
  cardStyle = styles.cardStyle,
  buttonModal = styles.buttonModal,
}) => {
  return (
    <View>
      <Modal isVisible={isVisible} style={modalStyle}>
        <View style={cardStyle}>
          <Image
            source={require("../assets/Icons/Alert.png")}
            resizeMode="contain"
            style={{
              height: "25%",
              alignSelf: "center",
            }}
          />
          <Text
            style={{
              textAlign: "center",
              fontFamily: "Comfortaa-Bold",
              color: "rgb(51,51,51)",
              margin: "5%",
            }}
          >
            {errorMessage}
          </Text>
          <TouchableOpacity
            style={[buttonModal, { height: "25%" }]}
            onPress={onPress}
            activeOpacity={0.6}
          >
            <Text
              style={{
                color: "white",
                textAlign: "center",
                fontSize: 15,
                fontFamily: "Comfortaa-Bold",
              }}
            >
              {i18n.t("common:retry")}
            </Text>
          </TouchableOpacity>
        </View>
      </Modal>
    </View>
  );
};

const styles = StyleSheet.create({
  modalStyle: {
    justifyContent: "center",
    alignItems: "center",
  },
  cardStyle: {
    height: hp("30%"),
    justifyContent: "center",
    width: "90%",
    padding: "5%",
    paddingBottom: 0,
    elevation: 6,
    backgroundColor: "white",
    borderRadius: 10,
  },
  buttonModal: {
    height: "10%",
    width: "100%",
    borderRadius: 8,
    justifyContent: "center",
    backgroundColor: "rgb(41,34,108)",
  },
});
