import {
  SET_BASIC_SETTINGS,
  SET_ADVANCE_SETTINGS,
  GET_JOBLIST
} from "../Actions/types";

const INITIAL_STATE = {
  joblist: [],
  jobDetails: ""
};

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case GET_JOBLIST:
      return {
        ...state,
        joblist: action.payload
      };
    case "GET_JOBDETAILS":
      return {
        ...state,
        jobDetails: action.payload
      };
    case "RESET_JOBDETAILS":
      return {
        ...state,
        jobDetails: ""
      };
    default:
      console.log("default called");
      return state;
  }
};
