import {
    QRHISTORY_LIST_DATA,
    QRHISTORY_LIST_ERROR,
    QRHISTORY_LOADER
} from "../Actions/types";
const INITIAL_STATE = {
    qrHistoryListData: "",
    qrHistoryListError: null,
    qrHistoryLoader: false,
    qrHistoryFail: false
};
export default (state = INITIAL_STATE, action) => {
    switch (action.type) {
        case QRHISTORY_LOADER:
            return {
                ...state,
                qrHistoryLoader: true,
            };
        case QRHISTORY_LIST_DATA:
            console.log('reducer called')
            return {
                ...state,
                qrHistoryListData: action.payload.reverse(),
                qrHistoryListError: null,
                qrHistoryLoader: false,
                qrHistoryFail: false
            };
        case QRHISTORY_LIST_ERROR:
            return {
                ...state,
                qrHistoryListError: action.payload,
                qrHistoryLoader: false,
                qrHistoryFail: true
            };
        default:
            return state;

    }
}