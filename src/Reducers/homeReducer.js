import { DASHBOARD_DATA, DASHBOARD_DATA_ERROR } from "../Actions/types";

const INITIAL_STATE = {
  dashboardData: null,
  dashboardDataError: null,
  loading: false,
  dashboardFail: false
};

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case DASHBOARD_DATA:
      return { ...state, dashboardData: action.payload, dashboardFail: false };
      case DASHBOARD_DATA_ERROR:
          console.log("DASHBOARD_DATA_ERROR", action.payload);
          return { ...state, dashboardDataError: action.payload, dashboardFail: true };
    default:
      return state;
  }
};
