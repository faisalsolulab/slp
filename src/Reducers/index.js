import { combineReducers } from "redux";
import AuthReducer from "./authReducer";
import BarrelReducer from "./barrelReducer";
import HomeReducer from "./homeReducer";
import ProductReducer from "./productsReducer";
import QrHistoryReducer from './qrhistoryReducer'
import UserProfile from './profileReducer'
import SprayParametersQuestion from './SprayParameterQuestion'
import SettingsReducer from "./SettingsReducer";

export default combineReducers({
  auth: AuthReducer,
  barrel: BarrelReducer,
  home: HomeReducer,
  products: ProductReducer,
  qrhistory: QrHistoryReducer,
  userprofile : UserProfile,
  TechSupport : SprayParametersQuestion,
  Settings : SettingsReducer
});
