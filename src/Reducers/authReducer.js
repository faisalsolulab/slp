import {
  EMAIL_CHANGED,
  PASSWORD_CHANGED,
  NAME_CHANGED,
  LAST_NAME_CHANGED,
  NUMBER_CHANGED,
  COMPANY_NAME_CHANGED,
  TERMS_CHECKED,
  TERMS_CHECKED_FALSE,
  LOGIN_USER_SUCCESS,
  LOGIN_USER_FAIL,
  LOGIN_USER,
  USER_PROFILE,
  REGISTER_USER,
  REGISTER_USER_FAIL,
  REGISTER_USER_SUCCESS,
  RESET_AUTH_DATA,
  ADD1_CHANGED,
  ADD2_CHANGED,
  CITY_CHANGED,
  STATE_CHANGED,
  ZIP_CHANGED,
  REFERRAL_CHANGED,
  SHOW_AUTH_MODAL,
  HIDE_AUTH_MODAL,
  EMAIL_LOG_CHANGED,
  PASS_LOG_CHANGED,
  COMPANY_LIST,
  AUTH_TOKEN_DATA,
} from "../Actions/types";

const INITIAL_STATE = {
  name: "",
  lastname: "",
  number: "",
  companyName: "",
  termsChecked: false,
  emailLog: "",
  passwordLog: "",
  email: "",
  password: "",
  add1: "",
  add2: "",
  city: "",
  state: "",
  zip: "",
  referral: "",
  user: null,
  error: null,
  errorRegister: null,
  loading: false,
  userData: null,
  accessToken: null,
  registerSuccess: false,
  fail: false,
  isModalVisible: false,
  companyList: [],
  registerLoading: false,
  userId: "",
};

export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case EMAIL_LOG_CHANGED:
      console.log("EMAIL_LOG_CHANGED", action.payload);
      return { ...state, emailLog: action.payload, fail: false };
    case PASS_LOG_CHANGED:
      console.log("PASS_LOG_CHANGED", action.payload);
      return { ...state, passwordLog: action.payload, fail: false };
    case EMAIL_CHANGED:
      console.log("EMAIL_CHANGED", action.payload);
      return { ...state, email: action.payload, fail: false };
    case ADD1_CHANGED:
      console.log("ADD1_CHANGED", action.payload);
      return { ...state, add1: action.payload, fail: false, success: false };
    case ADD2_CHANGED:
      console.log("ADD2_CHANGED", action.payload);
      return { ...state, add2: action.payload, fail: false };
    case CITY_CHANGED:
      console.log("CITY_CHANGED", action.payload);
      return { ...state, city: action.payload, fail: false };
    case STATE_CHANGED:
      console.log("STATE_CHANGED", action.payload);
      return { ...state, state: action.payload, fail: false };
    case ZIP_CHANGED:
      console.log("ZIP_CHANGED", action.payload);
      return { ...state, zip: action.payload, fail: false };
    case REFERRAL_CHANGED:
      console.log("REFERRAL_CHANGED", action.payload);
      return { ...state, referral: action.payload, fail: false };
    case PASSWORD_CHANGED:
      console.log("PASSWORD_CHANGED", action.payload);
      return { ...state, password: action.payload, fail: false };
    case NAME_CHANGED:
      console.log("NAME_CHANGED", action.payload);
      return { ...state, name: action.payload };
    case LAST_NAME_CHANGED:
      console.log("Last_NAME_CHANGED", action.payload);
      return { ...state, lastname: action.payload };
    case NUMBER_CHANGED:
      console.log("NUMBER_CHANGED", action.payload);
      return { ...state, number: action.payload };
    case COMPANY_NAME_CHANGED:
      console.log("COMPANY_NAME_CHANGED", action.payload);
      return { ...state, companyName: action.payload };
    case TERMS_CHECKED:
      console.log("TERMS_CHECKED", state.termsChecked);
      return { ...state, termsChecked: !state.termsChecked };
    case TERMS_CHECKED_FALSE:
      console.log("TERMS_CHECKED_FALSE", state.termsChecked);
      return { ...state, termsChecked: false };
    case LOGIN_USER:
      return { ...state, loading: true, error: "" };
    case REGISTER_USER:
      return {
        ...state,
        registerLoading: true,
        registerSuccess: false,
        errorRegister: null,
      };
    case LOGIN_USER_SUCCESS:
      console.log("LOGIN_USER_SUCCESS", action.payload);
      return {
        ...state,
        ...INITIAL_STATE,
        userData: action.payload,
        error: null,
        loading: false,
        accessToken: action.payload.access_token,
        userId: action.payload.user_id,
      };
    case REGISTER_USER_SUCCESS:
      console.log("REGISER SUCCESS");
      return {
        ...state,
        ...INITIAL_STATE,
        errorRegister: null,
        registerSuccess: true,
        registerLoading: false,
      };
    case AUTH_TOKEN_DATA:
      console.log("AUTH_TOKEN_DATA", action.payload);
      return {
        ...state,
        accessToken: action.payload.accessToken,
        userId: action.payload.userId,
      };
      case LOGIN_USER_FAIL:
        console.log("LOGIN_USER_FAIL", action.payload);
        return {
          ...state,
          passwordLog: "",
          fail: true,
          isModalVisible: true,
          error: action.payload,
          loading: false,
        };
    case REGISTER_USER_FAIL:
      return {
        ...state,
        errorRegister: action.payload,
        registerLoading: false,
        registerSuccess: false,
      };
    case USER_PROFILE:
      console.log("USER_PROFILE", action.payload);
      return { ...state, userData: action.payload };
    case SHOW_AUTH_MODAL:
      return { ...state, isModalVisible: true, fail: false };
    case HIDE_AUTH_MODAL:
      return { ...state, isModalVisible: false, fail: false };
    case COMPANY_LIST:
      console.log("COMPANY_LIST", action.payload);
      return { ...state, companyList: action.payload };
    case RESET_AUTH_DATA:
      console.log("RESET_AUTH_DATA");
      return { ...state, ...INITIAL_STATE };
    default:
      return state;
  }
};
