import {
  GET_PROFILE,
  GET_PROFILE_ERROR,
  SAVE_PROFILE,
  SAVE_PROFILE_LOADER,
  SNACK_RESET_PROFILE_UPDATE,
} from "../Actions/types";

const INITIAL_STATE = {
  get_profile_error: null,
  user_profile_data: null,
  loader: false,
  getProfileFail: false,
  getProfileSuccess: false,
};
export default (state = INITIAL_STATE, action) => {
  switch (action.type) {
    case GET_PROFILE:
      console.log("PROFILE reducer called");
      return {
        ...state,
        getProfileFail: false,
        user_profile_data: action.payload,
      };
    case SNACK_RESET_PROFILE_UPDATE:
      return {
        ...state,
        getProfileSuccess: false,
      };
    case GET_PROFILE_ERROR:
      console.log("errroror profile", action.payload);
      return {
        ...state,
        getProfileFail: true,
        get_profile_error: action.payload,
      };
    case SAVE_PROFILE:
      return {
        ...state,
        getProfileSuccess: true,
        user_profile_data: action.payload,
        loader: false,
      };
    case SAVE_PROFILE_LOADER:
      return {
        ...state,
        getProfileSuccess: false,
        loader: action.payload,
      };
    default:
      return state;
  }
};
