import { SET_BASIC_SETTINGS, SET_ADVANCE_SETTINGS } from "../Actions/types";

const INITIAL_STATE = {
  basicQuestions: [],
  advanceQuestions: []
};

export default (state = INITIAL_STATE, action) => {
  console.log("data....", state);
  switch (action.type) {
    case SET_BASIC_SETTINGS:
      const { qid, question, selected_option } = action.payload;
      let filterArray = [];
      if (state.basicQuestions.length > 0) {
        filterArray = state.basicQuestions.filter((item, index) => item.qid !== qid);
        filterArray.push(action.payload);
      } else {
        filterArray.push(action.payload);
      }
      return {
        ...state,
        basicQuestions: filterArray
      };
    case SET_ADVANCE_SETTINGS:
      return { ...state };
    default:
      console.log("default called");
      return { ...state };
  }
};
