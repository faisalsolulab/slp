import { SET_BASIC_SETTINGS } from "./types";
export const setBasicSettings = (value) => {
    return dispatch =>
      dispatch({
        type: SET_BASIC_SETTINGS,
        payload: value
      });
  };

export const setAdvanceSettings = (value) => {
    return dispatch =>
      dispatch({
        type: SET_BASIC_SETTINGS,
        payload: value
      });
  };