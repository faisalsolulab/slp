import axios from "axios";
import {
  SAVE_PROFILE,
  SAVE_PROFILE_ERROR,
  GET_PROFILE,
  GET_PROFILE_ERROR,
  SAVE_PROFILE_LOADER,
  SNACK_RESET_PROFILE_UPDATE,
} from "./types";
import { create } from "apisauce";
import { BASE_URL, ProfileApi } from "../config/api";
import AsyncStorage from "@react-native-community/async-storage";

const api = create({
  baseURL: BASE_URL,
});

export const resetSnackProfile = () => {
  return dispatch =>
    dispatch({
      type: SNACK_RESET_PROFILE_UPDATE,
    });
};

export const saveProfile = data => {
  return async dispatch => {
    await axios
      .put(ProfileApi.saveProfileApi(), data)
      .then(save_profile_data => {
        console.log("save_profile_data....", save_profile_data.data);
        dispatch({
          type: SAVE_PROFILE,
          payload: save_profile_data.data,
        });
      })
      .catch(err => {
        console.log("save_profile_data_error", err);
        dispatch({
          type: SAVE_PROFILE_ERROR,
          payload: err.response,
        });
      });
  };
};
export const getProfile = access_token => {
  return async dispatch => {
    await axios
      .get(ProfileApi.getProfileApi(access_token))
      .then(profiledata => {
        console.log("user pro file data....", profiledata.data);
        AsyncStorage.setItem("userData", JSON.stringify(profiledata.data.data));

        dispatch({
          type: GET_PROFILE,
          payload: profiledata.data,
        });
      })
      .catch(error => {
        console.log("get profile error", error);
        dispatch({
          type: GET_PROFILE_ERROR,
          payload: error.response !== undefined ? error.response.data.message : 'NETWORK ERROR' 
        });
      });
  };
};
export const enableLoader = () => {
  return dispatch => {
    dispatch({
      type: SAVE_PROFILE_LOADER,
      payload: true,
    });
  };
};
