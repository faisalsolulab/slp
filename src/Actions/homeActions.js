import axios from "axios";
import {
    DASHBOARD_DATA,
    DASHBOARD_DATA_ERROR
} from './types';
import { BASE_URL, homeApi } from "../config/api";

export const fetchDashBoard = (userToken) => {
    return async (dispatch) => {
        await axios.get(BASE_URL + homeApi.dashboard + userToken)
        .then((dashboardRes) => {
            console.log('dashboard', dashboardRes);
            dispatch({
                type: DASHBOARD_DATA,
                payload: dashboardRes.data.data
            });
        })
        .catch((err) => {
            console.log('dashboard error', err.response);
            dispatch({
                type: DASHBOARD_DATA_ERROR,
                payload: err.response !== undefined ? err.response.data.message : 'NETWORK ERROR' 
            });
        });
    };
};